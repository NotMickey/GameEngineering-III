﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class ObjectHealth : MonoBehaviourPunCallbacks, IHealth, IPunObservable {

    [SerializeField]
    private float current = 10f;
    public float Current { get { return current; } set { current = value; } }

    [SerializeField]
    private float max = 10f;
    public float Max { get { return max; } }

    public float Percent { get { return current / max; } }

    public bool IsAlive { get { return current > 0f; } set { IsAlive = value; } }
    public bool IsDead { get { return current <= 0f; } set { IsDead = value; } }

    public void Damage(IDamageInfo damageInfo)
    {
        if (IsDead)
        {
            Debug.LogWarningFormat("Attempting to damage dead unit {0}", this);
            return;
        }

        float actualDamage = Mathf.Min(current, damageInfo.Amount);
        float endHealth = current - actualDamage;

        DamageEvent damageEvent = new DamageEvent(this, damageInfo);

        if (Equals(actualDamage, 0f))
        {
            return;
        }

        damageEvent.HealthChange = -actualDamage;
        damageEvent.EndHealth = endHealth;

        ApplyHealthChange(damageEvent);

        if (IsDead)
        {
            DestroyObject();
        }
    }

    public void SetStats(IHealthStatsInfo stats)
    {
        HealthStatsEvent healthStatsEvent = new HealthStatsEvent(this, stats);
        healthStatsEvent.EndHealth = Mathf.Min(stats.Current, stats.Max);   // enforce that health cannot be greater than max health

        ApplyHealthChange(healthStatsEvent);
    }

    void DestroyObject()
    {
        Destroy(gameObject);
    }

    [System.Serializable]
    private class HealthEventCallback : UnityEvent<IHealthEvent> { }

    [SerializeField]
    private HealthEventCallback onHealthEvent = null;

    private void ApplyHealthChange(IHealthEvent healthEvent)
    {
        max = healthEvent.EndMaxHealth;
        current = healthEvent.EndHealth;

        if (onHealthEvent != null)
        {
            onHealthEvent.Invoke(healthEvent);
        }
    }

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			// Owned by this client
			stream.SendNext(Current);
		}
		else
		{
			// Network instance, receive data
			this.Current = (float)stream.ReceiveNext();
		}
	}
}
