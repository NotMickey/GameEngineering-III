﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ObjectSpawner : MonoBehaviourPunCallbacks
{

	[SerializeField]
	private Transform[] spawnPoints;

	[SerializeField]
	private GameObject debris;

	[SerializeField]
	private int size;

	private List<GameObject> spawnedObjects;

	private void Start()
	{
		spawnedObjects = new List<GameObject>();
	}

	public void SpawnDebris()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			int spawnNumber = Random.Range(0, size - 1);

			for (int i = 0; i < spawnNumber; i++)
			{
				spawnedObjects.Add(PhotonNetwork.Instantiate(debris.name, spawnPoints[i].position, Quaternion.identity));
			}
		}
	}

	public void RemoveDebris()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			foreach (GameObject curDebris in spawnedObjects)
			{
				PhotonNetwork.Destroy(curDebris);
			}

			spawnedObjects.Clear();
		}
	}

}
