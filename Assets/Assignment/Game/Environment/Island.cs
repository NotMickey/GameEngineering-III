﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Island : MonoBehaviour {

    [SerializeField]
    private Terrain terrain = null;

    [SerializeField]
    private Map map = null;

    [SerializeField]
    private float height = 0.1f;

    [SerializeField]
    private float maxRadius = 50;
    public float MaxRadius { get { return maxRadius; } }

    [SerializeField]
    private float falloffDist = 3f;

    [SerializeField]
    private AnimationCurve falloffCurve = null;

    [SerializeField]
    [HideInInspector]
    private float radius = 20f;
    public float Radius
    {
        get { return radius; }
        set
        {
            if (!Equals(radius, value))
            {
                radius = value;
                ChangeIslandSize(radius);

            }
        }
    }

    private void Start()
    {
        CreateRuntimeTerrain();
    }

    private void Reset()
    {
        ChangeIslandSize(radius);
    }

    private void ChangeIslandSize(float size)
    {
        float worldToSample = (terrain.terrainData.heightmapResolution / terrain.terrainData.size.x);
        int samples = Mathf.CeilToInt(2 * (maxRadius + falloffDist) * worldToSample);
        int offsetX = terrain.terrainData.heightmapWidth / 2 - samples / 2;
        int offsetY = terrain.terrainData.heightmapHeight / 2 - samples / 2;

        float[,] heights = terrain.terrainData.GetHeights(offsetX, offsetY, samples, samples);
        for (int x = 0; x < samples; x++)
        {
            for (int y = 0; y < samples; y++)
            {
                float dist = (new Vector2(x - samples / 2, y - samples / 2) / worldToSample).magnitude;
                //heights[x, y] = dist <= size ? height : 0;
                float t = Mathf.Clamp01((size - dist) / falloffDist);
                float h = falloffCurve.Evaluate(t) * height;
                heights[x, y] = h;
            }
        }

        terrain.terrainData.SetHeights(offsetX, offsetY, heights);

        //navMeshSurface.BuildNavMesh();
    }

    private void CreateRuntimeTerrain()
    {
        TerrainData newTerrainData = Instantiate<TerrainData>(terrain.terrainData);
        Terrain.activeTerrain.terrainData = newTerrainData;
        terrain.terrainData = newTerrainData;
        TerrainCollider terrainCollider = terrain.GetComponent<TerrainCollider>();
        terrainCollider.terrainData = newTerrainData;
    }

    public Vector3 GetRandomPosition()
    {
        Vector2 rand = Random.insideUnitCircle * radius;
        Vector3 pos = new Vector3(rand.x, 0, rand.y);
        Vector3 mapPos, mapNormal;
        map.GetMapPointFromWorldPoint(pos, out mapPos, out mapNormal);
        return mapPos;
    }


    //NOT USED


    //private IslandBrick[] bricks;

    //public IslandBrick[] Bricks
    //{
    //    get
    //    {
    //        return bricks;
    //    }

    //    set
    //    {
    //        bricks = value;
    //    }
    //}

    //[SerializeField]
    //private int islandLevels;

    //private int currentLevel;

    //private void Start()
    //{
    //    Bricks = GetComponentsInChildren<IslandBrick>();
    //    currentLevel = islandLevels;

    //    StopAllCoroutines();
    //    StartCoroutine(StartIslandDecay());
    //}

    //IEnumerator StartIslandDecay()
    //{
    //    yield return new WaitForSeconds(8.0f);

    //    while (currentLevel >= 0)
    //    {
    //        DissolveBricks(currentLevel);
    //        yield return new WaitForSeconds(8.0f);
    //        --currentLevel;
    //    }

    //    yield return null;
    //}

    //IEnumerator MoveToPosition(IslandBrick brick, Vector3 finalPosition, float time)
    //{
    //    float elaspedTime = 0.0f;
    //    Vector3 initialPosition = brick.transform.position;

    //    //Debug.Log("!!!");

    //    while (elaspedTime < time)
    //    {
    //        brick.transform.position = Vector3.Lerp(initialPosition, finalPosition, (elaspedTime / time));
    //        elaspedTime += Time.deltaTime * 0.2f;
    //        yield return null;
    //    }
    //}

    //void DissolveBricks(int level)
    //{
    //    Vector3 brickPosition;
    //    Vector3 finalPosition;

    //    foreach (IslandBrick brick in bricks)
    //    {
    //        if (brick.IslandLevel == level)
    //        {
    //            switch (brick.IslandSide)
    //            {
    //                case Side.Right:
    //                    brickPosition = brick.transform.position;
    //                    finalPosition = new Vector3(brickPosition.x - 100.0f, brickPosition.y - 50.0f, brickPosition.z);
    //                    StartCoroutine(MoveToPosition(brick, finalPosition, 5.0f));
    //                    break;

    //                case Side.Left:
    //                    brickPosition = brick.transform.position;
    //                    finalPosition = new Vector3(brickPosition.x + 100.0f, brickPosition.y - 50.0f, brickPosition.z);
    //                    StartCoroutine(MoveToPosition(brick, finalPosition, 5.0f));
    //                    break;

    //                case Side.Up:
    //                    brickPosition = brick.transform.position;
    //                    finalPosition = new Vector3(brickPosition.x, brickPosition.y - 50.0f, brickPosition.z - 100.0f);
    //                    StartCoroutine(MoveToPosition(brick, finalPosition, 5.0f));
    //                    break;

    //                case Side.Down:
    //                    brickPosition = brick.transform.position;
    //                    finalPosition = new Vector3(brickPosition.x, brickPosition.y - 50.0f, brickPosition.z + 100.0f);
    //                    StartCoroutine(MoveToPosition(brick, finalPosition, 5.0f));
    //                    break;

    //                default:
    //                    brickPosition = brick.transform.position;
    //                    finalPosition = new Vector3(brickPosition.x, brickPosition.y - 50.0f, brickPosition.z);
    //                    StartCoroutine(MoveToPosition(brick, finalPosition, 5.0f));
    //                    break;
    //            }
    //        }
    //    }
    //}
}
