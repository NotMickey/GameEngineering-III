﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Side { Up, Down, Left, Right, Center }

public class IslandBrick : MonoBehaviour {

    [SerializeField]
    private Side islandSide;

    [SerializeField]
    private int islandLevel;

    public Side IslandSide
    {
        get
        {
            return islandSide;
        }
    }

    public int IslandLevel
    {
        get
        {
            return islandLevel;
        }
    }
}
