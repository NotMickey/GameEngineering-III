﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Photon.Pun;

public class Game : MonoBehaviourPunCallbacks {

    [SerializeField]
    private int requiredRoundWins = 2;
    public int RequiredRoundWins { get { return requiredRoundWins; } }

    [SerializeField]
    private UIMessage outcomeMessage;

    [SerializeField]
    private List<Unit> units = null;

    [SerializeField]
    private Map map = null;
    public Map Map { get { return map; } }

    [SerializeField]
    private Transform roundsParent = null;

    [SerializeField]
    private Round roundPrefab = null;

    [SerializeField]
    private List<Round> rounds = null;

    public ShopUpgrades Shop { get { return shop; } }
    [SerializeField]
    private ShopUpgrades shop = null;

    [SerializeField]
    private ObjectSpawner debrisSpawner = null;

    [SerializeField]
    private App app = null;
    public App App { get { return app; } set { app = value; } }

    [System.Serializable]
    public class RoundChangeCallback : UnityEvent<RoundPhase> { }
    [HideInInspector]
    public RoundChangeCallback onRoundChangeEvent = null;

    [PunRPC]
    public void StartGame()
    {
        StartNewRound();

        // Give every player some starting gold
        GoldEventBus.Instance.onGoldBroadcastEvent.Invoke(10.0f);
    }

    void StartNewRound()
    {
        int roundNum = rounds.Count + 1;
        Round round = Instantiate<Round>(roundPrefab, roundsParent);
        round.name = string.Format("Round {0}", roundNum);
        rounds.Add(round);

        round.OnPhaseChanged.AddListener(OnRoundPhaseChange);

        Unit[] arrUnits = FindObjectsOfType<Unit>();

        Debug.Log("DID I find any units??" + arrUnits.Length);

        units = new List<Unit>();
        for (int i = 0; i < arrUnits.Length; ++i)
        {
            units.Add(arrUnits[i]);
        }

        round.Initialize(map, units);
        round.Invoke("OpenStore", 3f);
    }

    void OnRoundPhaseChange(Round round)
    {
        switch (round.Phase)
        {
            case RoundPhase.round_ready:
                OnPreRound(round);
                //photonView.RPC("OnPreRound", RpcTarget.All, round);
                break;
            case RoundPhase.round_inStore:
                OnStoreOpen(round);
                //photonView.RPC("OnStoreOpen", RpcTarget.All, round);
                break;
            case RoundPhase.round_inProgress:
                OnRoundStart(round);
                //photonView.RPC("OnRoundStart", RpcTarget.All, round);
                break;
            case RoundPhase.round_end:
                OnRoundOutcome(round);
                //photonView.RPC("OnRoundOutcome", RpcTarget.All, round);
                break;
            case RoundPhase.round_complete:
                OnRoundCompleted(round);
                //photonView.RPC("OnRoundCompleted", RpcTarget.All, round);
                break;
        }

        if (onRoundChangeEvent != null)
            onRoundChangeEvent.Invoke(round.Phase);
    }

    void OnPreRound(Round round)
    {
        outcomeMessage.ShowMessage(round.name, Color.black);
        outcomeMessage.UpdateUnits(units);
        debrisSpawner.SpawnDebris();
    }

    void OnStoreOpen(Round round)
    {
        outcomeMessage.HideMessage();

        foreach (Unit unit in units)
        {
            if (unit.Owner.IsLocal)
            {
                shop.gameObject.SetActive(true);

                shop.BuildShop(unit);
                //Debug.Log(unit.Owner + " has " + unit.Owner.Gold.CurrentGold);
                break;
            }
        }
    }

    public void CloseStoreManually()
    {
        rounds[rounds.Count - 1].CloseStore();
    }

    void OnRoundStart(Round round)
    {
        shop.gameObject.SetActive(false);

        Debug.Log("OnRoundStart!");

        foreach (Unit unit in units)
        {
            if (unit.Owner.IsLocal)
            {
                unit.Owner.GetComponent<UnitSelector>().SetDefaultFocus();
            }

            // Add control to all players
            if (!unit.Owner.gameObject.GetComponent<PlayerController>().IsControllable)
            {
                unit.Owner.gameObject.GetComponent<PlayerController>().IsControllable = true;
            }
        }
    }

    void OnRoundOutcome(Round round)
    {
        Player winner = round.Winner as Player;

        ShowRoundOutcome(winner);
        outcomeMessage.UpdateUnits(units);

        GoldEventBus.Instance.onGoldBroadcastEvent.Invoke(5.0f);
        GoldEventBus.Instance.GivePlayerGold(winner, 4.0f);
    }

    void OnRoundCompleted(Round round)
    {
        round.OnPhaseChanged.RemoveListener(OnRoundPhaseChange);

        debrisSpawner.RemoveDebris();

        Player winner = round.Winner as Player;

        if (winner != null)
        {
            Counter counter = winner.GetComponent<Counter>();
            if (counter == null)
            {
                counter = winner.gameObject.AddComponent<Counter>();
            }

            int roundWins = counter.Increment("roundWins");

            if (roundWins >= requiredRoundWins)
            {
                OnGameOver(winner);
            }
            else
            {
                StartNewRound();
            }
        }
    }

    public int GetRoundsWonCount(Player player)
    {
        int roundWins = 0;
        Counter counter = player.GetComponent<Counter>();
        if (counter != null)
        {
            roundWins = counter.Get("roundWins");
        }
        return roundWins;
    }

    void ShowRoundOutcome(Player winner)
    {
        if (winner != null)
        {
            if (winner.IsLocal)
            {
                outcomeMessage.ShowMessage(outcomeMessage.winRoundMessage, Color.green);
            }
            else
            {
                outcomeMessage.ShowMessage(string.Format(outcomeMessage.loseRoundMessage, winner.Name), Color.red);
            }
        }
        else
        {
            outcomeMessage.ShowMessage(outcomeMessage.tieMessage, Color.yellow);
        }
    }

    void OnGameOver(Player winner)
    {
        ShowGameOutcome(winner);

        StartCoroutine(GameOver_Coroutine());
    }

    void ShowGameOutcome(Player winner)
    {
        if (winner != null)
        {
            if (winner.IsLocal)
            {
                outcomeMessage.ShowMessage(outcomeMessage.winGameMessage, Color.green);
            }
            else
            {
                outcomeMessage.ShowMessage(string.Format(outcomeMessage.loseGameMessage, winner.Name), Color.red);
            }
        }
        else
        {
            outcomeMessage.ShowMessage(outcomeMessage.tieMessage, Color.yellow);
        }
    }

    IEnumerator GameOver_Coroutine()
    {

        yield return new WaitForSeconds(5f);

        if (app != null)
        {
            app.Scenes.GoToMainMenu();
        }

        Debug.Log("MENU!");
    }

    private void Awake()
    {
        if (onRoundChangeEvent == null)
            onRoundChangeEvent = new RoundChangeCallback();
    }

    private void OnDestroy()
    {
        onRoundChangeEvent.RemoveAllListeners();
    }
}
