﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameLoader : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private Game game;

	// Use this for initialization
	void Start ()
    {
        if (PhotonNetwork.IsMasterClient)
            Invoke("DelayedStart", 2.0f);
	}

    void DelayedStart()
    {
        photonView.RPC("StartNewGame", RpcTarget.All);
    }

    [PunRPC]
    void StartNewGame()
    {
        if (game != null)
        {
            game.StartGame();
        }
    }
}
