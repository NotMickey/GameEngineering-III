﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class Round : MonoBehaviourPunCallbacks, IRound<RoundPhase,RoundEvent>, IHealthSource {

    [SerializeField]
    private RoundPhase phase = RoundPhase.round_ready;
    public RoundPhase Phase { get { return phase; } }

    [SerializeField]
    private RoundEvent onPhaseChanged = null;
    public RoundEvent OnPhaseChanged { get { return onPhaseChanged; } }

    [SerializeField]
    private Player winner = null;
    public IPlayer Winner { get { return winner; } }

    [SerializeField]
    private List<Unit> allUnits = null;

    public List<IUnit> AliveUnits
    {
        get
        {
            return allUnits.FindAll((u) => u.Health.IsAlive).ConvertAll<IUnit>((u) => u as IUnit);
        }
    }

    public List<IPlayer> AlivePlayers
    {
        get
        {
            return allUnits.FindAll((u) => u.Health.IsAlive).ConvertAll<IPlayer>((u) => u.Owner);
        }
    }

    [SerializeField]
    private Map map = null;

    [SerializeField]
    private float timePerDecay = 10f;

    [SerializeField]
    private float amountPerDecay = 5f;

    public void Initialize(Map map, List<Unit> units)
    {
        allUnits = new List<Unit>(units);
        this.map = map;

        phase = RoundPhase.round_ready;
        OnPhaseChanged.Invoke(this);

        map.SetIslandRadius(25f);    // TODO: data driven
        PrepUnits();
        map.Lava.Clear();

    }

    public void OpenStore()
    {
        phase = RoundPhase.round_inStore;
        OnPhaseChanged.Invoke(this);

        Debug.Log("Store opened");

        StartCoroutine(StoreTime(15.0f));
    }

    public void CloseStore()
    {
        StopAllCoroutines();

        StartRound();

        Debug.Log("Store closed");
    }

    public void StartRound()
    {
        phase = RoundPhase.round_inProgress;
        OnPhaseChanged.Invoke(this);

        StartCoroutine(DecayIsland(map.Island));
    }

    void PrepUnits()
    {
        foreach (Unit unit in allUnits)
        {
            HealthStatsInfo healthStatsInfo = new HealthStatsInfo(this, unit.Health.Max, unit.Health.Max);
            unit.Health.SetStats(healthStatsInfo);

            int i = allUnits.IndexOf(unit);
            Transform spawnPoint = map.GetSpawnPoint(i);

            if (PhotonNetwork.IsMasterClient)
            {
                spawnPoint = map.GetSpawnPoint(0);
                unit.transform.position = spawnPoint.position;
                unit.transform.rotation = spawnPoint.rotation;
            }
            else
            {
                unit.transform.position = spawnPoint.position;
                unit.transform.rotation = spawnPoint.rotation;
            }

            if (unit.Controller != null)
            {
                unit.Controller.StopAll();
            }

            // Remove control from all players
            if (unit.Owner.gameObject.GetComponent<PlayerController>().IsControllable)
            {
                unit.Owner.gameObject.GetComponent<PlayerController>().IsControllable = false;
            }

            // Reset animations
            unit.GetComponent<Animator>().Rebind();
        }
    }

    IEnumerator StoreTime(float time)
    {
        yield return new WaitForSeconds(time);

        CloseStore();

        yield break;
    }

    IEnumerator DecayIsland(Island island)
    {

        while (island.Radius > 0)
        {

            yield return new WaitForSeconds(timePerDecay);

            float newRadius = Mathf.Max(0f, island.Radius - amountPerDecay);
            map.SetIslandRadius(newRadius);

        }

    }

    void Update()
    {
        switch (phase)
        {
            case RoundPhase.round_inProgress:
                Update_InProgress();
                break;
        }
    }

    void Update_InProgress()
    {
        List<IPlayer> alivePlayers = AlivePlayers;
        if (alivePlayers.Count <= 1)
        {
            winner = alivePlayers.Count > 0 ? alivePlayers[0] as Player : null;
            StartCelebration();
        }
    }

    void StartCelebration()
    {
        phase = RoundPhase.round_end;
        onPhaseChanged.Invoke(this);
        StopAllCoroutines();
        StartCoroutine(Celebration_Coroutine());
    }

    void EndRound()
    {
        phase = RoundPhase.round_complete;
        onPhaseChanged.Invoke(this);
    }

    IEnumerator Celebration_Coroutine()
    {
        yield return new WaitForSeconds(3f);
        EndRound();
    }
}
