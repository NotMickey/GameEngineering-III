﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoundPhase {

    round_ready,
    round_inStore,
    round_inProgress,
    round_end,
    round_complete
}
