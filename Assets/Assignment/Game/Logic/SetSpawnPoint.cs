﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSpawnPoint : MonoBehaviour {

    [SerializeField]
    private Transform spawnPoint;

	// Use this for initialization
	void Start ()
    {
        transform.position = spawnPoint.position;
	}
	
}
