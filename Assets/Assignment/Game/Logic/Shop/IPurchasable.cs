﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPurchasable
{
    string ItemName { get; }
    float ItemCost { get; set; }
    string ItemDiscription { get; }

    void TryPurchase();

    void PopulateTooltip();
}
