﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpgradePurchasable
{
    int UpgradeLevel { get; set; }

    void OnUpgradePurchased(float newCost);
}
