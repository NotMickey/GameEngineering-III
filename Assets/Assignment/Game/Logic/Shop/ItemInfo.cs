﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum ItemType { AbilityItem, InventoryItem}

public class ItemInfo : MonoBehaviour, IPurchasable, IUpgradePurchasable, IPointerEnterHandler
{
    #region UI

    [SerializeField]
    private Text abilityName;

    [SerializeField]
    private Image[] abilityLevels;

    [SerializeField]
    private Sprite acquired;

    [SerializeField]
    private Sprite notAcquired;

    [SerializeField]
    private Text costText;

    #endregion

    [System.Serializable]
    public class PurchaseEventCallback : UnityEvent<Unit, string, float, ItemType> { }
    [HideInInspector]
    public PurchaseEventCallback onPurchaseEvent = null;
    
    [System.Serializable]
    public class DynamicTooltipCallback : UnityEvent<string, int, ItemType> { }
    [HideInInspector]
    public DynamicTooltipCallback onTooltipUpdated = null;

    public string ItemName { get { return itemName; } }

    public float ItemCost { get { return itemCost; } set { itemCost = value; } }

    public int UpgradeLevel { get { return upgradeLevel; } set { upgradeLevel = value; } }

    public string ItemDiscription { get { return itemDescription; } }

    public ItemType Type { get { return type; } set { type = value; } }

    public Unit ItemOwner { get { return itemOwner; } set { itemOwner = value; } }

    public void TryPurchase()
    {
        onPurchaseEvent.Invoke(itemOwner, ItemName, ItemCost, Type);
    }

    public void OnUpgradePurchased(float newCost)
    {
        // If player purchased this upgrade, modify UI and set level to next one

        if (UpgradeLevel == 3)
            return;

        SetLevel(UpgradeLevel + 1);
        SetUpgradeCost(newCost);
        PopulateTooltip();
    }

    public void SetLevel(int level)
    {
        switch (level)
        {
            case 0:
                upgradeLevel = 0;
                break;

            case 1:
                abilityLevels[0].sprite = acquired;
                upgradeLevel = 1;
                break;

            case 2:
                abilityLevels[0].sprite = acquired;
                abilityLevels[1].sprite = acquired;
                upgradeLevel = 2;
                break;

            case 3:
                abilityLevels[0].sprite = acquired;
                abilityLevels[1].sprite = acquired;
                abilityLevels[2].sprite = acquired;
                upgradeLevel = 3;
                break;

            default:
                break;
        }
    }

    public void SetName(string name)
    {
        itemName = name;
        abilityName.text = name;
    }

    public void SetDescription(string description)
    {
        itemDescription = description;
    }

    public void SetUpgradeCost(float cost)
    {
        itemCost = cost;
        costText.text = cost.ToString();
    }

    public void SetType(ItemType itemType)
    {
        type = itemType;
    }

    private void Awake()
    {
        onPurchaseEvent = new PurchaseEventCallback();
        onTooltipUpdated = new DynamicTooltipCallback();
    }

    private void OnDestroy()
    {
        onPurchaseEvent.RemoveAllListeners();
        onTooltipUpdated.RemoveAllListeners();
    }

    public void PopulateTooltip()
    {
        Tooltip.Instance.ClearFields();

        // Set common elements
        CommonElements common = new CommonElements
        {
            name = ItemName,
            description = itemDescription
        };
        Tooltip.Instance.SetCommonElements(common);

        // Set upgrade steps
        onTooltipUpdated.Invoke(ItemName, UpgradeLevel, type);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PopulateTooltip();
    }

    private Unit itemOwner;
    private string itemName;
    private int upgradeLevel;
    private float itemCost;
    private string itemDescription;
    private ItemType type;
}
