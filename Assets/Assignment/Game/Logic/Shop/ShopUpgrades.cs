﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ShopUpgrades : MonoBehaviour {

    [SerializeField]
    private GameObject ShopAbilityParentPanel;

    [SerializeField]
    private GameObject ShopItemParentPanel;

    [SerializeField]
    private GameObject AbilityInfo;

    [SerializeField]
    private GameObject ItemInfo;

    [SerializeField]
    public UpgradeSystem upgrades;

    [SerializeField]
    private Text goldAmount;

    //public Unit TrackedUnit { get { return trackedUnit; } }
    //private Unit trackedUnit;

    List<GameObject> AbilityItems;
    List<GameObject> InventoryItems;

    public void BuildShop(Unit localUnit)
    {
        //trackedUnit = localUnit;

        int AbilityCount = upgrades.AbilityCount();

        for (int i = 0; i < AbilityCount; i++)
        {
            GameObject ability = Instantiate(AbilityInfo, ShopAbilityParentPanel.transform);

            ability.GetComponent<ItemInfo>().SetName(upgrades.GetAbilityName(i));

            int abilityLevel = localUnit.abilityHandler.GetAbilityLevelByName(upgrades.GetAbilityName(i));
            string abilityDescription = upgrades.GetAbilityDescription(upgrades.GetAbilityName(i), abilityLevel);

            ability.GetComponent<ItemInfo>().SetLevel(abilityLevel);
            ability.GetComponent<ItemInfo>().SetUpgradeCost(upgrades.GetCostOfAbilityUpgrade(upgrades.GetAbilityName(i), abilityLevel + 1));
            ability.GetComponent<ItemInfo>().SetDescription(abilityDescription);
            ability.GetComponent<ItemInfo>().SetType(ItemType.AbilityItem);
            ability.GetComponent<ItemInfo>().ItemOwner = localUnit;

            ability.GetComponent<ItemInfo>().onPurchaseEvent.AddListener(AttmeptPurchase);
            ability.GetComponent<ItemInfo>().onTooltipUpdated.AddListener(UpdateTooltipUI);

            //Debug.Log(ability);

            AbilityItems.Add(ability);
        }

        int ItemCount = upgrades.ItemCount();

        for (int i = 0; i < ItemCount; i++)
        {
            GameObject item = Instantiate(ItemInfo, ShopItemParentPanel.transform);

            item.GetComponent<ItemInfo>().SetName(upgrades.GetItemName(i));

            int itemLevel = localUnit.unitInventory.GetItemLevelByName(upgrades.GetItemName(i));
            string itemDescription = upgrades.GetItemDescription(upgrades.GetItemName(i), itemLevel);

            item.GetComponent<ItemInfo>().Type = ItemType.InventoryItem;

            item.GetComponent<ItemInfo>().SetLevel(itemLevel);
            item.GetComponent<ItemInfo>().SetUpgradeCost(upgrades.GetCostOfItemUpgrade(upgrades.GetItemName(i), itemLevel + 1));
            item.GetComponent<ItemInfo>().SetDescription(itemDescription);
            item.GetComponent<ItemInfo>().ItemOwner = localUnit;

            item.GetComponent<ItemInfo>().onPurchaseEvent.AddListener(AttmeptPurchase);
            item.GetComponent<ItemInfo>().onTooltipUpdated.AddListener(UpdateTooltipUI);

            InventoryItems.Add(item);
        }

        goldAmount.text = localUnit.Owner.Gold.CurrentGold.ToString();
    }

    public void DestroyShop()
    {
        foreach (GameObject abilityItem in AbilityItems)
        {
            Destroy(abilityItem);
        }

        AbilityItems.Clear();

        foreach (GameObject inventoryItem in InventoryItems)
        {
            Destroy(inventoryItem);
        }

        InventoryItems.Clear();
    }

    /// <summary>
    /// This function is specifiaclly used by the AI to look for and attempt to purchase an item or ability
    /// </summary>
    /// <param name="trackedUnit"></param>
    /// <param name="name"></param>
    /// <param name="itemType"></param>
    public void PurchaseItemByNameAndType(Unit trackedUnit, string name, ItemType itemType)
    {
        if (itemType == ItemType.AbilityItem)
        {
            int currentLevel = trackedUnit.abilityHandler.GetAbilityLevelByName(name);

            float cost = upgrades.GetCostOfAbilityUpgrade(name, currentLevel + 1);

            AttmeptPurchaseAI(trackedUnit, name, cost, itemType);
        }

        if (itemType == ItemType.InventoryItem)
        {
            int currentLevel = trackedUnit.unitInventory.GetItemLevelByName(name);

            float cost = upgrades.GetCostOfItemUpgrade(name, currentLevel + 1);

            AttmeptPurchaseAI(trackedUnit, name, cost, itemType);
        }
    }

    private void AttmeptPurchase(Unit trackedUnit, string name, float cost, ItemType itemType)
    {
        // Check if player has enough gold!
        if (!GoldEventBus.Instance.CheckForGold(trackedUnit.Owner, cost))
        {
            Debug.Log("Not enough gold for purchase!");
            return;
        }

        if (itemType == ItemType.AbilityItem)
        {
            int currentLevel = trackedUnit.abilityHandler.GetAbilityLevelByName(name);

            if (currentLevel == 3)
                return;

            GoldEventBus.Instance.SubtractGold(trackedUnit.Owner, cost);

            GameObject abilityPrefab = upgrades.GetAbilityPrefab(name, currentLevel + 1);

            //trackedUnit.abilityHandler.UpgradeAbility(abilityPrefab, name);

            PhotonView photonView = trackedUnit.GetComponent<PhotonView>();

            int unitID = photonView.ViewID;

            photonView.RPC("UpgradeAbilityNetwork", RpcTarget.All, name, currentLevel + 1, unitID);

            if (!trackedUnit.Owner.IsLocal)
                return;

            for (int i = 0; i < upgrades.AbilityCount(); i++)
            {
                if (AbilityItems[i].GetComponent<ItemInfo>().ItemName == name)
                {
                    AbilityItems[i].GetComponent<ItemInfo>().OnUpgradePurchased(upgrades.GetCostOfAbilityUpgrade(name, currentLevel + 2));
                }
            }
        }

        if (itemType == ItemType.InventoryItem)
        {
            int currentLevel = trackedUnit.unitInventory.GetItemLevelByName(name);

            if (currentLevel == 3)
                return;

            GoldEventBus.Instance.SubtractGold(trackedUnit.Owner, cost);

            GameObject itemPrefab = upgrades.GetItemPrefab(name, currentLevel + 1);

            trackedUnit.unitInventory.UpgradeItem(itemPrefab, name);

            PhotonView photonView = trackedUnit.GetComponent<PhotonView>();

            int unitID = photonView.ViewID;
   
            //photonView.RPC("UpgradeItemNetwork", RpcTarget.All, name, currentLevel + 1, unitID);

            if (!trackedUnit.Owner.IsLocal)
                return;

            for (int i = 0; i < upgrades.ItemCount(); i++)
            {
                if (InventoryItems[i].GetComponent<ItemInfo>().ItemName == name)
                {
                    InventoryItems[i].GetComponent<ItemInfo>().OnUpgradePurchased(upgrades.GetCostOfItemUpgrade(name, currentLevel + 2));
                }
            }
        }

        goldAmount.text = trackedUnit.Owner.Gold.CurrentGold.ToString();
    }

    private void AttmeptPurchaseAI(Unit trackedUnit, string name, float cost, ItemType itemType)
    {
        // Check if player has enough gold!
        if (!GoldEventBus.Instance.CheckForGold(trackedUnit.Owner, cost))
        {
            Debug.Log("Not enough gold for purchase!");
            return;
        }

        if (itemType == ItemType.AbilityItem)
        {
            int currentLevel = trackedUnit.abilityHandler.GetAbilityLevelByName(name);

            if (currentLevel == 3)
                return;

            GoldEventBus.Instance.SubtractGold(trackedUnit.Owner, cost);

            GameObject abilityPrefab;

            // THIS IS WHAT HAPPENS WHEN YOU DON'T DESIGN STUFF PROPERLY!!!

            if (name == "Teleport" || name == "Meteor" || name == "GravityWell" || name == "Fireball" || name == "Tether")
            {
                abilityPrefab = upgrades.GetAbilityPrefab(name, currentLevel + 3 + 1);
            }
            else
            {
                abilityPrefab = upgrades.GetAbilityPrefab(name, currentLevel + 1);
            }

            trackedUnit.abilityHandler.UpgradeAbility(abilityPrefab, name);

            if (!trackedUnit.Owner.IsLocal)
                return;

            for (int i = 0; i < upgrades.AbilityCount(); i++)
            {
                if (AbilityItems[i].GetComponent<ItemInfo>().ItemName == name)
                {
                    AbilityItems[i].GetComponent<ItemInfo>().OnUpgradePurchased(upgrades.GetCostOfAbilityUpgrade(name, currentLevel + 2));
                }
            }
        }

        if (itemType == ItemType.InventoryItem)
        {
            int currentLevel = trackedUnit.unitInventory.GetItemLevelByName(name);

            if (currentLevel == 3)
                return;

            GoldEventBus.Instance.SubtractGold(trackedUnit.Owner, cost);

            GameObject itemPrefab = upgrades.GetItemPrefab(name, currentLevel + 1);

            trackedUnit.unitInventory.UpgradeItem(itemPrefab, name);

            if (!trackedUnit.Owner.IsLocal)
                return;

            for (int i = 0; i < upgrades.ItemCount(); i++)
            {
                if (InventoryItems[i].GetComponent<ItemInfo>().ItemName == name)
                {
                    InventoryItems[i].GetComponent<ItemInfo>().OnUpgradePurchased(upgrades.GetCostOfItemUpgrade(name, currentLevel + 2));
                }
            }
        }

        goldAmount.text = trackedUnit.Owner.Gold.CurrentGold.ToString();
    }

    private void UpdateTooltipUI(string name, int level, ItemType itemType)
    {
        // Set upgrade steps
        UpgradeSteps steps = new UpgradeSteps(level);

        if (itemType == ItemType.AbilityItem)
        {
            steps.costs.Add(upgrades.GetCostOfAbilityUpgrade(name, 1));
            steps.costs.Add(upgrades.GetCostOfAbilityUpgrade(name, 2));
            steps.costs.Add(upgrades.GetCostOfAbilityUpgrade(name, 3));
        }

        if (itemType == ItemType.InventoryItem)
        {
            steps.costs.Add(upgrades.GetCostOfItemUpgrade(name, 1));
            steps.costs.Add(upgrades.GetCostOfItemUpgrade(name, 2));
            steps.costs.Add(upgrades.GetCostOfItemUpgrade(name, 3));
        }

        Tooltip.Instance.SetUprgadeSteps(steps);
    }

    private void OnDisable()
    {
        DestroyShop();
    }

    private void Awake()
    {
        AbilityItems = new List<GameObject>();
        InventoryItems = new List<GameObject>();
    }
}
