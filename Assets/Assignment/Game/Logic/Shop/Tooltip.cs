﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct CommonElements
{
    public Sprite sprite;
    public string name;
    public string description;
}

public struct DynamicElements
{
    public float cooldown;
    public float range;
    public float damage;
}

public struct UpgradeSteps
{
    public List<int> costs;
    public int currentLevel;

    public UpgradeSteps(int level)
    {
        currentLevel = level;
        costs = new List<int>();
    }
}

public class Tooltip : MonoBehaviour {

    public static Tooltip Instance { get { return _instance; } }

    [SerializeField]
    Image image;

    [SerializeField]
    Text objectName;

    [SerializeField]
    Text description;

    [SerializeField]
    Text cooldown;

    [SerializeField]
    Text range;

    [SerializeField]
    Text damage;

    [SerializeField]
    Text currentLevel;

    public void ClearFields()
    {
        image.sprite = null;

        objectName.text = "";

        description.text = "";

        cooldown.text = "-";

        range.text = "-";

        damage.text = "-";

        currentLevel.text = "";
    }

    public void SetCommonElements(CommonElements elements)
    {
        if (elements.sprite != null)
            image.sprite = elements.sprite;
        objectName.text = elements.name;
        description.text = elements.description;
    }

    public void SetDynamicElements(DynamicElements elements)
    {
        if (elements.cooldown != 0f)
            cooldown.text = elements.cooldown.ToString();

        if (elements.range != 0f)
            range.text = elements.range.ToString();

        if (elements.damage != 0f)
            damage.text = elements.damage.ToString();
    }

    public void SetUprgadeSteps(UpgradeSteps steps)
    {
        currentLevel.supportRichText = true;

        switch (steps.currentLevel)
        {
            case 0:
                currentLevel.text = steps.costs[0] + "/" + steps.costs[1] + "/" + steps.costs[2];
                break;

            case 1:
                currentLevel.text = "<b>"+steps.costs[0]+"</b>" + "/" + steps.costs[1] + "/" + steps.costs[2];
                break;

            case 2:
                currentLevel.text = steps.costs[0] + "/" + "<b>"+steps.costs[1]+"</b>" + "/" + steps.costs[2];
                break;

            case 3:
                currentLevel.text = steps.costs[0] + "/" + steps.costs[1] + "/" + "<b>"+steps.costs[2]+"</b>";
                break;

            default:
                break;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private static Tooltip _instance;
}
