﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class helps in calculating utility for AI decisions by providing helper functions.
/// </summary>
public class UtilityHelper : MonoBehaviour
{
    [SerializeField]
    private Transform mapCenter = null;

    public Transform MapCenter { get { return mapCenter; } }

    /// <summary>
    /// Returns nearest unit belonging to enemy player within given radius
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public Unit GetNearestUnit(Unit unit, float radius)
    {
        Unit closestUnit = null;
        float minDist = Mathf.Infinity;

        // Detect within a 50m radius
        Collider[] hitColliders = Physics.OverlapSphere(unit.transform.position, radius);

        foreach (Collider collider in hitColliders)
        {
            Unit detectedUnit = collider.GetComponent<Unit>();

            if (detectedUnit == null || detectedUnit.Owner == unit.Owner)
                continue;

            float dist = Vector3.Distance(detectedUnit.transform.position, unit.transform.position);

            if (dist < minDist)
            {
                closestUnit = detectedUnit;
                minDist = dist;
            }
        }

        return closestUnit;
    }
	
    /// <summary>
    /// Returns the specified units current health value
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public float GetCurrentHealth(Unit unit)
    {
        return unit.GetComponent<Health>().Current;
    }

    /// <summary>
    /// Returns the specified units maximum health value
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public float GetMaxHealth(Unit unit)
    {
        return unit.GetComponent<Health>().Max;
    }

    /// <summary>
    /// Gets a random point on the ground in a certain radius around the given unit. 
    /// </summary>
    /// <param name="unit"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public Vector3 GetRandomPointAroundUnit(Unit unit, float radius)
    {
        Vector3 position = Random.insideUnitCircle * radius;

        position += unit.transform.position;

        RaycastHit raycastHit;

        Debug.DrawRay(position + new Vector3(0.0f, 200.0f, 0.0f), Vector3.down);

        if (Physics.Raycast(position + new Vector3(0.0f, 40.0f, 0.0f), Vector3.down, out raycastHit))
        {
            if (raycastHit.collider.tag == "Walkable")
            {
                return raycastHit.point;
            }
        }

        // Recursive! can be dangerous!!!
        return GetRandomPointAroundUnit(unit, radius);
    }

    /// <summary>
    /// Similar to the function above but returns a random point in the vicinity of the given location
    /// </summary>
    /// <param name="location"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public Vector3 GetRandomPointAroundLocation(Vector3 location, float radius)
    {
        Vector3 position = Random.insideUnitCircle * radius;

        position += location;

        RaycastHit raycastHit;

        if (Physics.Raycast(position + new Vector3(0.0f, 40.0f, 0.0f), Vector3.down, out raycastHit))
        {
            if (raycastHit.collider.tag == "Walkable")
            {
                return raycastHit.point;
            }
        }

        // Recursive! can be dangerous!!!
        return GetRandomPointAroundLocation(location, radius);
    }
}
