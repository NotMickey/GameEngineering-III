﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIPlayerController : PlayerController
{
	[SerializeField]
	private Game gameInstance = null;

	[SerializeField]
	private UtilityHelper utilityHelper = null;

	[SerializeField]
	[HideInInspector]
	private Vector3 targetLocation;

	[SerializeField]
	[HideInInspector]
	private Vector3 targetDirection;

	[SerializeField]
	[HideInInspector]
	private UnitAbilityHandler abilityHandler;

	[SerializeField]
	[HideInInspector]
	private bool isCasting = false;

	[SerializeField]
	[HideInInspector]
	private Unit thisUnit = null;

	[SerializeField]
	[HideInInspector]
	private UnitController thisUnitController = null;

	[SerializeField]
	[HideInInspector]
	private bool useProtect = false;

	private void Start()
	{
		SkillCastEventBus.Instance.OnSkillCast.AddListener(DetectAbilityCast);
		gameInstance.onRoundChangeEvent.AddListener(RoundChangeListener);

		thisUnit = GetComponentInChildren<Unit>();
		thisUnitController = thisUnit.Controller as UnitController;
		abilityHandler = thisUnit.abilityHandler;
	}

	// Returns location the AI has targeted for whatever reason
	override public Vector3 TargetPosition()
	{
		return targetLocation;
	}

	// Returns direction the AI has targeted for whatever reason
	public override Vector3 TargetDirection()
	{
		return targetDirection;
	}

	/// <summary>
	/// Use this to make purchases when game is in store mode and start or stop AI logic
	/// </summary>
	/// <param name="phase"></param>
	void RoundChangeListener(RoundPhase phase)
	{
		if (phase == RoundPhase.round_inStore)
		{
			//Debug.Log("AI making purchases!");
			StartCoroutine(PurchaseLogic());
		}
		else if (phase == RoundPhase.round_inProgress)
		{
			StartCoroutine(MakeDecision());
        }
		else if (phase == RoundPhase.round_end)
		{
            StopAllCoroutines();
		}
	}

	IEnumerator PurchaseLogic()
	{
		int minLevel;
		int minCost;
		Player thisPlayer = GetComponent<Player>();

		while (true)
		{
			minLevel = 5;
			minLevel = Mathf.Min(minLevel, thisUnit.abilityHandler.GetAbilityLevelByName("Detonate"));
			minLevel = Mathf.Min(minLevel, thisUnit.unitInventory.GetItemLevelByName("Scepter"));
			minLevel = Mathf.Min(minLevel, thisUnit.unitInventory.GetItemLevelByName("Amulet"));
			minLevel = Mathf.Min(minLevel, thisUnit.abilityHandler.GetAbilityLevelByName("Teleport"));
			minLevel = Mathf.Min(minLevel, thisUnit.abilityHandler.GetAbilityLevelByName("SpellShield"));

			if (minLevel == 3)
				yield break;

			minCost = 9999;
			minCost = Mathf.Min(minCost, gameInstance.Shop.upgrades.GetCostOfAbilityUpgrade("Detonate", thisUnit.abilityHandler.GetAbilityLevelByName("Detonate") + 1));
			minCost = Mathf.Min(minCost, gameInstance.Shop.upgrades.GetCostOfItemUpgrade("Scepter", thisUnit.unitInventory.GetItemLevelByName("Scepter") + 1));
			minCost = Mathf.Min(minCost, gameInstance.Shop.upgrades.GetCostOfItemUpgrade("Amulet", thisUnit.unitInventory.GetItemLevelByName("Amulet") + 1));
			minCost = Mathf.Min(minCost, gameInstance.Shop.upgrades.GetCostOfAbilityUpgrade("Teleport", thisUnit.abilityHandler.GetAbilityLevelByName("Teleport") + 1));
			minCost = Mathf.Min(minCost, gameInstance.Shop.upgrades.GetCostOfAbilityUpgrade("SpellShield", thisUnit.abilityHandler.GetAbilityLevelByName("SpellShield") + 1));

			if (minCost > thisPlayer.Gold.CurrentGold)
				yield break;

			if (thisUnit.abilityHandler.GetAbilityLevelByName("Detonate") == minLevel)
			{
				gameInstance.Shop.PurchaseItemByNameAndType(thisUnit, "Detonate", ItemType.AbilityItem);
				yield return new WaitForSeconds(0.1f);
			}

			if (thisUnit.unitInventory.GetItemLevelByName("Scepter") == minLevel)
			{
				gameInstance.Shop.PurchaseItemByNameAndType(thisUnit, "Scepter", ItemType.InventoryItem);
				yield return new WaitForSeconds(0.1f);
			}

			if (thisUnit.unitInventory.GetItemLevelByName("Amulet") == minLevel)
			{
				gameInstance.Shop.PurchaseItemByNameAndType(thisUnit, "Amulet", ItemType.InventoryItem);
				yield return new WaitForSeconds(0.1f);
			}

			if (thisUnit.abilityHandler.GetAbilityLevelByName("Teleport") == minLevel)
			{
				gameInstance.Shop.PurchaseItemByNameAndType(thisUnit, "Teleport", ItemType.AbilityItem);
				yield return new WaitForSeconds(0.1f);
			}

			if (thisUnit.abilityHandler.GetAbilityLevelByName("SpellShield") == minLevel)
			{
				gameInstance.Shop.PurchaseItemByNameAndType(thisUnit, "SpellShield", ItemType.AbilityItem);
				yield return new WaitForSeconds(0.1f);
			}

			yield return null;
		}
	}

	/// <summary>
	/// Use this to detect abilities cast by other players
	/// </summary>
	/// <param name="owner"></param>
	/// <param name="ability"></param>
	void DetectAbilityCast(Unit owner, Ability ability)
	{
		if (owner.Owner != GetComponent<Player>())
		{
			// Someone else cast this ability!
			Debug.Log("Detected a foreign ability");
			UseProtect(ability);
		}
		else
		{
			// reset is casting
			isCasting = false;
		}
	}

	void UseProtect(Ability ability)
	{
		if (!isCasting)
		{
			if ((ability.AbilityName == "Fireball" || ability.AbilityName == "Meteor"))
			{
				if (abilityHandler.GetAbilityLevelByName("SpellShield") > 0)
				{
					if (!GetComponentInChildren<SpellShield>().GetComponent<Cooldown>().IsOnCoolDown)
					{
						useProtect = true;
					}
				}
			}
		}
	}

	float SelfPreservationUtility()
	{
		float utility = 0;
		float healthFraction = thisUnit.Health.Percent;

		Debug.Log("health = " + healthFraction);

		utility += Mathf.Pow(0.1f, healthFraction);

		if (useProtect)
			utility += 1;

		return utility;
	}

	float AggressivenessUtility()
	{
		float utility = 0;
		float healthFraction = thisUnit.Health.Percent;

		utility += Mathf.Pow(healthFraction, 2);

		Unit enemyUnit = utilityHelper.GetNearestUnit(thisUnit, 30.0f);
		if (enemyUnit)
		{
			healthFraction = enemyUnit.Health.Percent;

			utility += Mathf.Pow(0.1f, healthFraction);
			//utility += Vector3.Distance(enemyUnit.transform.position, thisUnit.transform.position) / 30.0f;
			utility /= 2;
		}

		return utility;
	}

	IEnumerator MakeDecision()
	{
		float selfPreservation, Aggressiveness;

		while (true)
		{
			yield return new WaitForSeconds(2);

			selfPreservation = SelfPreservationUtility();
			Aggressiveness = AggressivenessUtility();

			if (isCasting || thisUnit.Health.IsDead)
				yield return null;

			Debug.Log(selfPreservation + "  " + Aggressiveness);

			if (selfPreservation >= Aggressiveness)
			{
				if (useProtect)
				{
					abilityHandler.ExecuteAbility(abilityHandler.GetAbilityNumberByName("SpellShield"));
					isCasting = true;
					useProtect = false;
				}
				else
				{
					// Simple AI tries to move towards center of map or away from enemy
					Unit unit = utilityHelper.GetNearestUnit(thisUnit, 1000.0f);
					Vector3 position = utilityHelper.GetRandomPointAroundLocation(utilityHelper.MapCenter.position, 4.0f);

					if (Vector3.Dot(unit.transform.position, position) > 0.7)
					{
						// distance between center and enemy is less than tolerance, do nothing
						if (Vector3.Distance(unit.transform.position, position) < 10.0f)
							yield return null;
					}

					// Check if unit has teleport ability
					if (abilityHandler.GetAbilityLevelByName("Teleport") > 0)
					{
						// Check if ability is on cooldown ?
						if (!GetComponentInChildren<Teleport>().GetComponent<Cooldown>().IsOnCoolDown)
						{
							targetLocation = position; // Set this before casting any ability with point targeting!
							abilityHandler.ExecuteAbility(abilityHandler.GetAbilityNumberByName("Teleport"));

							// Need to call this because of reasons
							OnClickedCallback.Invoke();

							isCasting = true;
						}
						else
							thisUnitController.MoveTo(position);
					}
					else
						thisUnitController.MoveTo(position);
				}
			}
			else
			{
				if (useProtect)
					useProtect = false;

				// Check if unit has detonate ability 
				if (abilityHandler.GetAbilityLevelByName("Detonate") > 0)
				{
					// Check if ability is on cooldown?
					if (!GetComponentInChildren<Detonate>().GetComponent<Cooldown>().IsOnCoolDown)
					{
						// Check if enemy unit in range?
						Unit unit = utilityHelper.GetNearestUnit(thisUnit, 10.0f);

						if (unit != null)
						{
							// Attack!
							abilityHandler.ExecuteAbility(abilityHandler.GetAbilityNumberByName("Detonate"));
							isCasting = true;
						}
						else
						{
							unit = utilityHelper.GetNearestUnit(thisUnit, 1000.0f);

							if (unit != null)
							{
								// Move closer to unit
								thisUnitController.MoveTo(utilityHelper.GetRandomPointAroundUnit(unit, 3.0f));
							}
						}
					}
					else
					{
						Unit unit = utilityHelper.GetNearestUnit(thisUnit, 1000.0f);

						if (unit != null)
						{
							// Move closer to enemy unit
							thisUnitController.MoveTo(utilityHelper.GetRandomPointAroundUnit(unit, 3.0f));
						}
					}
				}
				else
				{
					// OH NO!!!!
				}
			}

			yield return null;
		}
	}
}
