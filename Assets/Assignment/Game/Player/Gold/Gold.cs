﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour {

    public float CurrentGold { get { return currentGold; } set { currentGold = value; } }

    public void AddGold(float amount)
    {
        currentGold += amount;
        //Debug.Log("Added " + amount + "gold for " + GetComponent<Player>().Name);
    }

	// Use this for initialization
	void Start ()
    {
        if (GoldEventBus.Instance != null)
        {
            GoldEventBus.Instance.onGoldBroadcastEvent.AddListener(AddGold);
            //Debug.Log("Added gold listener for " + GetComponent<Player>().Name);
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level == 6)
        {
            if (GoldEventBus.Instance != null)
            {
                GoldEventBus.Instance.onGoldBroadcastEvent.AddListener(AddGold);
                //Debug.Log("Added gold listener for " + GetComponent<Player>().Name);
            }
        }
    }

    private float currentGold;
}
