﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GoldEventBus : MonoBehaviour {

    [System.Serializable]
    public class GoldBroadcastEventCallback : UnityEvent<float> { }
    [NonSerialized]
    public GoldBroadcastEventCallback onGoldBroadcastEvent = null;

    [System.Serializable]
    public class GoldEventCallback : UnityEvent<float,Player> { }
    [NonSerialized]
    public GoldEventCallback onGoldEvent = null;

    public static GoldEventBus Instance
    {
        get
        {
            return _instance;
        }
    }

    private static GoldEventBus _instance = null;

    public void GivePlayerGold(Player player, float amount)
    {
        if (player == null)
            return;

        player.Gold.AddGold(amount);
    }

    public bool CheckForGold(Player player, float amount)
    {
        if (player == null)
            return false;

        if (player.Gold.CurrentGold >= amount)
        {
            return true;
        }

        return false;
    }

    public void SubtractGold(Player player, float amount)
    {
        if (player == null)
            return;

        player.Gold.CurrentGold -= amount;
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }

        onGoldBroadcastEvent = new GoldBroadcastEventCallback();
        onGoldEvent = new GoldEventCallback();
    }

    private void OnDestroy()
    {
        onGoldBroadcastEvent.RemoveAllListeners();
        onGoldEvent.RemoveAllListeners();
    }
}
