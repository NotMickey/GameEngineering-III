﻿using UnityEngine;

using Photon.Pun;

public class LocalPlayerController : PlayerController {

    [SerializeField]
    private BasicMovementController controller = null;

    public override void Awake()
    {
        base.Awake();
    }

    override public void ResetMovementControl()
    {
        OnClickedCallback.RemoveAllListeners();
        OnClickedCallback.AddListener(controller.MouseClicked);
        FirstSkillCallback.AddListener(controller.FirstSkill);
        SecondSkillCallback.AddListener(controller.SecondSkill);
        ThirdSkillCallback.AddListener(controller.ThirdSkill);
        FourthSkillCallback.AddListener(controller.FourthSkill);
        FifthSkillCallback.AddListener(controller.FifthSkill);
    }

    override public void RemoveMovementControl()
    {
        OnClickedCallback.RemoveAllListeners();
        FirstSkillCallback.RemoveAllListeners();
        SecondSkillCallback.RemoveAllListeners();
        ThirdSkillCallback.RemoveAllListeners();
        FourthSkillCallback.RemoveAllListeners();
        FifthSkillCallback.RemoveAllListeners();
    }

    private void Start()
    {
        if (controller != null)
        {
            OnClickedCallback.AddListener(controller.MouseClicked);
            FirstSkillCallback.AddListener(controller.FirstSkill);
            SecondSkillCallback.AddListener(controller.SecondSkill);
            ThirdSkillCallback.AddListener(controller.ThirdSkill);
            FourthSkillCallback.AddListener(controller.FourthSkill);
            FifthSkillCallback.AddListener(controller.FifthSkill);
        }

        if (photonView.IsMine)
            IsControllable = true;
        else
            IsControllable = false;
    }

    private void Update()
    {
        if (IsControllable)
        {
            if (Input.GetMouseButton(0))
            {
                if (OnClickedCallback != null)
                {
                    OnClickedCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.Q) && Player.IsLocal)
            {
                if (FirstSkillCallback != null)
                {
                    FirstSkillCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.W) && Player.IsLocal)
            {
                if (SecondSkillCallback != null)
                {
                    SecondSkillCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.E) && Player.IsLocal)
            {
                if (ThirdSkillCallback != null)
                {
                    ThirdSkillCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.R) && Player.IsLocal)
            {
                if (FourthSkillCallback != null)
                {
                    FourthSkillCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.T) && Player.IsLocal)
            {
                if (FifthSkillCallback != null)
                {
                    FifthSkillCallback.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.S) && Player.IsLocal)
            {
                foreach (Unit unit in Units)
                {
                    unit.Controller.StopAll();
                    ResetMovementControl();
                }
            }
        }
    }
}
