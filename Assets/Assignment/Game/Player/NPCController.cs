﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

        RaycastHit[] floor;
        floor = Physics.RaycastAll(transform.parent.position + new Vector3(0.0f, 10.0f, 0.0f), transform.up * -1);

        foreach (RaycastHit hit in floor)
        {
            if (hit.collider.tag == "Walkable")
            {
                Debug.Log(hit.point);

                Vector3 offset = new Vector3(transform.parent.position.x, hit.point.y, transform.parent.position.z);

                if ((hit.point.y >= (transform.parent.position.y + 0.2f)) || (hit.point.y <= (transform.parent.position.y - 0.2f)))
                {
                    transform.parent.position = offset;
                }
            }
        }
    }
}
