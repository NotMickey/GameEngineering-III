﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class Player : MonoBehaviourPunCallbacks, IPlayer, IPunObservable {

#region IPlayer
    public Color Color { get { return color; } }
    public bool IsLocal { get { return isLocal; } }
    public string Name { get { return playerName; } set { playerName = value; } }
    #endregion

    public Gold Gold { get { return playerGold; } }

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance = null;

    [SerializeField]
    private Color color;
    [SerializeField]
    private bool isLocal;
    [SerializeField]
    private string playerName;
    [SerializeField]
    private Gold playerGold;


    private void Awake()
    {
        if (photonView.IsMine)
        {
            isLocal = true;
            LocalPlayerInstance = this.gameObject;

            Debug.Log("Local player set to " + LocalPlayerInstance);
        }  
        else
            isLocal = false;

        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        Renderer renderer = GetComponentInChildren<Renderer>();
        renderer.material.SetColor("_Color", Color);
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level == 1)
        {
            // Returned to main menu!
            Destroy(gameObject);
        }

        if (photonView.IsMine)
        {
            isLocal = true;
            LocalPlayerInstance = this.gameObject;
        }
        else
            isLocal = false;

        playerGold.AddGold(-playerGold.CurrentGold);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(playerName);
        }
        else
        {
            playerName = (string)stream.ReceiveNext();
        }
    }
}
