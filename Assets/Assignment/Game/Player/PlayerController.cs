﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using Photon.Pun;

public class PlayerController : MonoBehaviourPunCallbacks, IPlayerController {

#region IPlayerController
    public IPlayer Player { get { return player; } }
    public List<IUnit> Units { get { return units; } }
    #endregion

    public bool IsControllable { get { return isControllable; } set { isControllable = value; } }

    [HideInInspector]
    public UnityEvent OnClickedCallback = new UnityEvent();
    [HideInInspector]
    public UnityEvent FirstSkillCallback = new UnityEvent();
    [HideInInspector]
    public UnityEvent SecondSkillCallback = new UnityEvent();
    [HideInInspector]
    public UnityEvent ThirdSkillCallback = new UnityEvent();
    [HideInInspector]
    public UnityEvent FourthSkillCallback = new UnityEvent();
    [HideInInspector]
    public UnityEvent FifthSkillCallback = new UnityEvent();

    private IPlayer player;
    private List<IUnit> units;
    private bool isControllable;

    virtual public void ResetMovementControl()
    { }

    virtual public void RemoveMovementControl()
    { }

    virtual public Vector3 TargetPosition()
    { return new Vector3(); }

    virtual public Vector3 TargetDirection()
    { return new Vector3(); }

    public virtual void Awake()
    {
        Unit[] list = GetComponentsInChildren<Unit>();
        units = new List<IUnit>();

        foreach (Unit unit in list)
        {
            units.Add(unit);
        }

        player = GetComponent<Player>();
    }
}
