﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BasicMovementController : MonoBehaviour {

    public Unit activeUnit;

    [SerializeField]
    private UnitSelector unitSelector;

    private Ray clickRay;
    private RaycastHit clickHit;
    
    public void MouseClicked()
    {
        //Debug.Log("Mouse click");

        if (!EventSystem.current.IsPointerOverGameObject())
        {
            clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(clickRay.origin, clickRay.direction, out clickHit))
            {
                if (clickHit.collider.tag == "Walkable")
                {
                    if (activeUnit.Health.IsAlive && activeUnit.Owner.IsLocal)
                        activeUnit.Controller.MoveTo(clickHit.point);
                }
                else if (clickHit.collider.tag == "Player")
                {
                    Unit clickedUnit = clickHit.collider.gameObject.GetComponent<Unit>();

                    //if (clickedUnit.Name != activeUnit.Name)
                    //{
                    //    unitSelector.RemoveFocus();
                    //    unitSelector.RemoveControl();

                    //    unitSelector.AddFocus(clickedUnit);

                    //    activeUnit = clickedUnit;

                    //    //if (activeUnit.Owner == this.GetComponent<Player>())
                    //    //{
                    //    //    unitSelector.AddControl();
                    //    //}

                    //    if (activeUnit.Owner .IsLocal)
                    //    {
                    //        unitSelector.AddControl();
                    //    }
                    //}
                    if (clickedUnit.Owner != activeUnit.Owner)
                    {
                        unitSelector.RemoveFocus();
                        unitSelector.RemoveControl();

                        unitSelector.AddFocus(clickedUnit);

                        activeUnit = clickedUnit;

                        //if (activeUnit.Owner == this.GetComponent<Player>())
                        //{
                        //    unitSelector.AddControl();
                        //}

                        if (activeUnit.Owner.IsLocal)
                        {
                            unitSelector.AddControl();
                        }
                    }
                }
            }
        }    
    }

    public void FirstSkill()
    {
        if (activeUnit.Owner == GetComponent<Player>())
        {
            //execute first ability -- TODO: make adding new abilites more streamlined

            if (activeUnit.abilityHandler.abilities.Count > 0)
                activeUnit.abilityHandler.ExecuteAbility(0);
        }
    }

    public void SecondSkill()
    {
        if (activeUnit.Owner == GetComponent<Player>())
        {
            //execute first ability -- TODO: make adding new abilites more streamlined

            if (activeUnit.abilityHandler.abilities.Count > 1)
                activeUnit.abilityHandler.ExecuteAbility(1);
        }
    }

    public void ThirdSkill()
    {
        if (activeUnit.Owner == GetComponent<Player>())
        {
            //execute first ability -- TODO: make adding new abilites more streamlined

            if (activeUnit.abilityHandler.abilities.Count > 2)
                activeUnit.abilityHandler.ExecuteAbility(2);
        }
    }

    public void FourthSkill()
    {
        if (activeUnit.Owner == GetComponent<Player>())
        {
            //execute first ability -- TODO: make adding new abilites more streamlined

            if (activeUnit.abilityHandler.abilities.Count > 3)
                activeUnit.abilityHandler.ExecuteAbility(3);
        }
    }

    public void FifthSkill()
    {
        if (activeUnit.Owner == GetComponent<Player>())
        {
            //execute first ability -- TODO: make adding new abilites more streamlined

            if (activeUnit.abilityHandler.abilities.Count > 4)
                activeUnit.abilityHandler.ExecuteAbility(4);
        }
    }
}
