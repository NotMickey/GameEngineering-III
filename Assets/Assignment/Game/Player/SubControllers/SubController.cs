﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControllerType { Exclusive, Shared}

public class SubController : MonoBehaviour {

    protected Player owner = null;
    protected ControllerType type = ControllerType.Exclusive;

}
