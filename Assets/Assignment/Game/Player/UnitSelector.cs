﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class UnitSelector : MonoBehaviour {

    [SerializeField]
    private CameraFollow focusCam;

    [SerializeField]
    private HealthUI UIHealthBar;

    [SerializeField]
    private Text UIInfo;

    [SerializeField]
    private SkillBar skillBar;

    [SerializeField]
    private Unit focusedUnit;
    public Unit FocusedUnit { get { return focusedUnit; }  set { focusedUnit = value; } }

    public PhotonView photonView = null;

    public void RemoveFocus()
    {
        if (focusedUnit == null)
            return;

        focusedUnit.GetComponent<Health>().onHealthUIEvent.RemoveListener(UIHealthBar.UpdateUIScreenSpace);
        focusedUnit.gameObject.transform.GetChild(0).gameObject.SetActive(false);

        if (skillBar == null)
            return;

        skillBar.ClearSkillBar();
    }
    
    public void RemoveControl()
    {
        //Debug.Log("Removed control from " + focusedUnit.Name + " owned by " + focusedUnit.Owner);
    }

    public void AddFocus(Unit i_clickedUnit)
    {
        focusedUnit = i_clickedUnit;

        //Debug.Log("Added control to " + focusedUnit.Name + " owned by " + focusedUnit.Owner);

        if (focusCam != null)
        {
            //Update focus camera
            focusCam.unitTransform = FocusedUnit.transform;
        }

        if (UIHealthBar != null)
        {
            //Update health bar on global UI
            focusedUnit.GetComponent<Health>().onHealthUIEvent.AddListener(UIHealthBar.UpdateUIScreenSpace);
            UIHealthBar.unitHealthInfo = focusedUnit.GetComponent<Health>();
            UIHealthBar.UpdateUIScreenSpace();
        }

        if (UIInfo != null)
        {
            //Update Player info on global UI
            UIInfo.text = focusedUnit.Owner.Name;
        }

        if (skillBar != null)
        {
            //Update skill info on global UI
            skillBar.UpdateSkillBar(focusedUnit);
        }

        //Display Local UI
        focusedUnit.gameObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void AddControl()
    {
        //Debug.Log("Controlling " + focusedUnit.Name + " owned by " + focusedUnit.Owner);

        if (skillBar != null)
        {
            //Add controls for skills
            skillBar.AddListeners();
        }
    }

    public void SetDefaultFocus()
    {
        if (focusedUnit == null)
        {
            focusedUnit = GetComponentInChildren<Unit>();
        }

        RemoveFocus();

        AddFocus(focusedUnit);
        AddControl();
    }

    private void Start()
    {
        photonView = GetComponent<PhotonView>();

        focusCam = FindObjectOfType<CameraFollow>();
        UIHealthBar = GameObject.FindGameObjectWithTag("WorldUI").GetComponent<HealthUI>();
        skillBar = FindObjectOfType<SkillBar>();
        UIInfo = GameObject.FindGameObjectWithTag("PlayerName").GetComponent<Text>();

        if (photonView.IsMine)
        {
            SetDefaultFocus();
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        photonView = GetComponent<PhotonView>();

        focusCam = FindObjectOfType<CameraFollow>();
        UIHealthBar = GameObject.FindGameObjectWithTag("WorldUI").GetComponent<HealthUI>();
        skillBar = FindObjectOfType<SkillBar>();
        UIInfo = GameObject.FindGameObjectWithTag("PlayerName").GetComponent<Text>();

        if (photonView.IsMine)
        {
            SetDefaultFocus();
        }
    }
}
