﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private float panSpeed = 20.0f;

    [SerializeField]
    private float panBorderThickness = 10.0f;

    [SerializeField]
    private Vector2 panMinLimit;

    [SerializeField]
    private Vector2 panMaxLimit;

    [SerializeField]
    private bool bCanScroll;

    [SerializeField]
    private float scrollSpeed = 2.0f;

    [SerializeField]
    private Vector2 scrollRange;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 currentPosition = transform.position;

        if (Input.GetKey(KeyCode.UpArrow) || (Input.mousePosition.y >= Screen.height - panBorderThickness))
        {
            currentPosition.z -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow) || (Input.mousePosition.y <= panBorderThickness))
        {
            currentPosition.z += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.RightArrow) || (Input.mousePosition.x >= Screen.width - panBorderThickness))
        {
            currentPosition.x -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.LeftArrow) || (Input.mousePosition.x <= panBorderThickness))
        {
            currentPosition.x += panSpeed * Time.deltaTime;
        }

        currentPosition.x = Mathf.Clamp(currentPosition.x, panMinLimit.x, panMaxLimit.x);
        currentPosition.z = Mathf.Clamp(currentPosition.z, panMinLimit.y, panMaxLimit.y);

        if (bCanScroll)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            currentPosition.y -= scroll * scrollSpeed * Time.deltaTime * 100.0f;

            currentPosition.y = Mathf.Clamp(currentPosition.y, scrollRange.x, scrollRange.y);
        }
        
        transform.position = currentPosition;
	}
}
