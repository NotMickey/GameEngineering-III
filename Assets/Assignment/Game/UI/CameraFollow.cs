﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform unitTransform = null;

    [SerializeField]
    private float smoothSpeed = 1.0f;

    [SerializeField]
    private Vector3 cameraOffset;

    private void LateUpdate()
    {
        if (unitTransform != null)
            transform.position = unitTransform.position + cameraOffset;
    }

}
