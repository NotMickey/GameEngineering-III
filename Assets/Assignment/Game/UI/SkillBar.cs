﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SkillBar : MonoBehaviour {

    [NonSerialized]
    public List<GameObject> skillList = new List<GameObject>();

    [NonSerialized]
    public Unit focusedUnit;

    [SerializeField]
    private GameObject skillButtonPrefab = null;

	// Use this for initialization
	void Start ()
    {
        if (skillButtonPrefab == null)
        {
            Debug.LogWarning("Reference to skillButton prefab missing! Please assign one manually.");
        }
	}
	
	public void UpdateSkillBar(Unit i_activeUnit)
    {
        focusedUnit = i_activeUnit;

        foreach (Ability ability in focusedUnit.abilityHandler.abilities)
        {
            GameObject skill = Instantiate(skillButtonPrefab, transform);
            skill.GetComponentInChildren<Text>().text = ability.AbilityName;

            skillList.Add(skill);
        }
    }

    public void AddListeners()
    {
        if (skillList.Count != 0 && focusedUnit.abilityHandler.abilities.Count != 0)
        {
            for (int i = 0; i < focusedUnit.abilityHandler.abilities.Count; i++)
            {
                int x = i;

                skillList[i].GetComponent<Button>().onClick.AddListener(() => focusedUnit.abilityHandler.ExecuteAbility(x));
            }
        }
    }

    public void ClearSkillBar()
    {
        if (skillList.Count != 0)
        {
            foreach (GameObject skill in skillList)
            {
                skill.GetComponent<Button>().onClick.RemoveAllListeners();

                Destroy(skill);
            }

            skillList.Clear();
        }
    }

}
