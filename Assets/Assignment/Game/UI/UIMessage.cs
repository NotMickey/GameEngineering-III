﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class UIMessage : MonoBehaviour {

    public string winRoundMessage = "You won the round!";
    public string loseRoundMessage = "{0} won the round";
    public string tieMessage = "It's a tie";

    public string winGameMessage = "You won the game!";
    public string loseGameMessage = "{0} won the game";

    public string winsMessage = "{0}/{1} wins";

    [SerializeField]
    private Text messageLabel = null;

    [SerializeField]
    private Text player1Wins = null;

    [SerializeField]
    private Text player2Wins = null;

    [SerializeField]
    private Game game = null;

    public void ShowMessage(string message, Color? color = null)
    {
        messageLabel.text = message;
        if (color.HasValue)
        {
            messageLabel.color = color.Value;
        }
        messageLabel.gameObject.SetActive(true);
        messageLabel.transform.parent.gameObject.SetActive(true);
    }

    public void HideMessage()
    {
        messageLabel.gameObject.SetActive(false);
        messageLabel.transform.parent.gameObject.SetActive(false);
    }

    public void UpdateUnits(List<Unit> units)
    {
        for (int i = 0; i < units.Count; i++)
        {
            Unit unit = units[i];
            int wins = game.GetRoundsWonCount(unit.Owner);
            string status = string.Format(winsMessage, wins, game.RequiredRoundWins);

            //if (units[i].Owner.Name == "Player1")
            //    player1Wins.text = "Player1 : " + status;
            //else
            //    player2Wins.text = "Player2 : " + status;

            if (i == 0)
                player1Wins.text = units[i].Owner.Name + " : " + status;
            else
                player2Wins.text = units[i].Owner.Name + " : " + status;
        }
    }
}
