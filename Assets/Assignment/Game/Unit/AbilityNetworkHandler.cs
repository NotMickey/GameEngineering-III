﻿using UnityEngine;
using Photon.Pun;

public class AbilityNetworkHandler : MonoBehaviour
{
    // This is a hack
    [PunRPC]
	public void DetonateAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<Detonate>().DetonateAllOverNetwork();
    }

    [PunRPC]
    public void FireballAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<Fireball>().LaunchFireballNetwork();
    }

    [PunRPC]
    public void TeleportAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<Teleport>().TeleportOverNetwork();
    }

    [PunRPC]
    public void TetherAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<Tether>().CastThetherNetwork();
    }

    [PunRPC]
    public void MeteorAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<Meteor>().LaunchFireballNetwork();
    }

    [PunRPC]
    public void GravityWellAbilityNetwork(Vector3 wellLocation, int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<GravityWell>().SpawnWellNetworked(wellLocation);
    }

    [PunRPC]
    public void SpellShieldAbilityNetwork(int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GetComponentInChildren<SpellShield>().ActivateShieldNetworked();
    }
}
