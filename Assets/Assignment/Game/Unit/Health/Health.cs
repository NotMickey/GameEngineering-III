﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using Photon.Pun;

public class Health : MonoBehaviourPunCallbacks, IHealth, IPunObservable {

    #region IDamageable

    [SerializeField]
    private float current = 100f;
    public float Current
    {
        get { return current; } set { current = value; }
    }

    [SerializeField]
    private float max = 100f;
    public float Max { get { return max; } }

    public float Percent { get { return current / max; } }

    public bool IsAlive { get { return current > 0f; } set { IsAlive = value; } }
    public bool IsDead { get { return current <= 0f; } set { IsDead = value; } }

    public void Damage(IDamageInfo damageInfo)
    {

        if (IsDead)
        {
            Debug.LogWarningFormat("Attempting to damage dead unit {0}", this);
            return;
        }

        DamageEvent damageEvent;

        float actualDamage = Mathf.Min(current, damageInfo.Amount);
        float endHealth;

        if (damageInfo.DamageType == DamageType.Heal)
        {
            endHealth = current + actualDamage;

            damageEvent = new DamageEvent(this, damageInfo);

            damageEvent.HealthChange = actualDamage;
        }
        else
        {
            if (Equals(actualDamage, 0f) || isInvincible)
            {
                return;
            }

            actualDamage = GetComponent<Unit>().attributes.ModifyIncomingDamage(actualDamage);
            endHealth = current - actualDamage;

            damageEvent = new DamageEvent(this, damageInfo);

            damageEvent.HealthChange = -actualDamage;
        }
        
        damageEvent.EndHealth = endHealth;

        ApplyHealthChange(damageEvent);

        if (IsDead)
        {
            OnDeath();
            if (damageAttributer != null && damageAttributer != GetComponentInParent<Player>())
            {
                GoldEventBus.Instance.GivePlayerGold(damageAttributer, 2.0f);
            }
        }
    }

    public void SetStats(IHealthStatsInfo stats)
    {
        HealthStatsEvent healthStatsEvent = new HealthStatsEvent(this, stats);
        healthStatsEvent.EndHealth = Mathf.Min(stats.Current, stats.Max);   // enforce that health cannot be greater than max health

        ApplyHealthChange(healthStatsEvent);
    }


    #endregion

    [System.Serializable]
    private class HealthEventCallback : UnityEvent<IHealthEvent> { }

    [SerializeField]
    private HealthEventCallback onHealthEvent = null;

    public UnityEvent onHealthUIEvent = new UnityEvent();

    public Player damageAttributer = null;

    public bool IsInvincible
    {
        get { return isInvincible; }
        set { isInvincible = value; }
    }

    [HideInInspector]
    private bool isInvincible = false;

    #region Helpers

    private void ApplyHealthChange(IHealthEvent healthEvent)
    {

        max = healthEvent.EndMaxHealth;
        current = healthEvent.EndHealth;

        if (onHealthEvent != null)
        {
            onHealthEvent.Invoke(healthEvent);
           
        }

        if (onHealthUIEvent != null)
        {
            onHealthUIEvent.Invoke();
        }

    }

    private void OnDeath()
    {
        Animator unitAnimator = GetComponent<Animator>();

        unitAnimator.ResetTrigger("IsChanneling");
        unitAnimator.SetTrigger("HasDied");
    }

    #endregion

    private void RegisterDamageAttribution(Player damagingPlayer, Unit damagedUnit)
    {
        if (damagedUnit == GetComponent<Unit>())
        {
            damageAttributer = damagingPlayer;
        }
    }

    private void Start()
    {
        if (KillAttributionEventBus.Instance != null)
            KillAttributionEventBus.Instance.onDamageEvent.AddListener(RegisterDamageAttribution);
    }

    private void OnLevelWasLoaded(int level)
    {
        if (KillAttributionEventBus.Instance != null)
            KillAttributionEventBus.Instance.onDamageEvent.AddListener(RegisterDamageAttribution);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // Owned by this client

            stream.SendNext(IsInvincible);
            stream.SendNext(Current);
            stream.SendNext(damageAttributer);
        }
        else
        {
            // Network instance, recieve data

            this.IsInvincible = (bool)stream.ReceiveNext();
            this.Current = (float)stream.ReceiveNext();

            if (onHealthUIEvent != null)
            {
                onHealthUIEvent.Invoke();
            }

            this.damageAttributer = (Player)stream.ReceiveNext();
        }
    }
}
