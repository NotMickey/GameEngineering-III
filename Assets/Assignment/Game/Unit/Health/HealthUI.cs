﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    [SerializeField]
    private GameObject UI;

    [SerializeField]
    private Image healthBar;

    [SerializeField]
    public Health unitHealthInfo;

    private Camera playerCamera;

    private void Start()
    {
        playerCamera = Camera.main;
    }

    public void UpdateUI()
    {
        if (unitHealthInfo != null)
            healthBar.fillAmount = unitHealthInfo.Percent;
    }

    public void UpdateUIScreenSpace()
    {
        if (unitHealthInfo != null)
            healthBar.fillAmount = unitHealthInfo.Percent;
    }

    private void LateUpdate()
    {
        // make UI face camera
        if (UI != null)
        {
            if (playerCamera == null)
                playerCamera = Camera.main;

            UI.transform.LookAt(UI.transform.position + playerCamera.transform.rotation * Vector3.forward,
            playerCamera.transform.rotation * Vector3.up);
        }  
    }
}
