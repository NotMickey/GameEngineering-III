﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KillAttributionEventBus : MonoBehaviour {

    [System.Serializable]
    public class DamageEventCallback : UnityEvent<Player,Unit> { }
    [NonSerialized]
    public DamageEventCallback onDamageEvent = null;

    public static KillAttributionEventBus Instance { get { return _instance; } }

    private static KillAttributionEventBus _instance = null;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }

        onDamageEvent = new DamageEventCallback();
    }

    private void OnDestroy()
    {
        onDamageEvent.RemoveAllListeners();
    }
}
