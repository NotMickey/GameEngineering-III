﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdolls : MonoBehaviour {

    public Transform pelvis;
    public Transform Lhips;
    public Transform Lknee;
    public Transform Lfoot;
    public Transform Rhips;
    public Transform Rknee;
    public Transform Rfoot;
    public Transform middleSpine;
    public Transform head;
    public Transform Lupperarm;
    public Transform Lforearm;
    public Transform Rupperarm;
    public Transform Rforearm;

    Vector3 pelvisPosition;
    Vector3 LhipsPosition;
    Vector3 LkneePosition;
    Vector3 LfootPosition;
    Vector3 RhipsPosition;
    Vector3 RkneePosition;
    Vector3 RfootPosition;
    Vector3 middleSpinePosition;
    Vector3 headPosition;
    Vector3 LupperarmPosition;
    Vector3 LforearmPosition;
    Vector3 RupperarmPosition;
    Vector3 RforearmPosition;

    private void Start()
    {
        GetChildPositions();
    }

    private void OnDisable()
    {
        ResetRagdoll();
    }

    public void ResetRagdoll()
    {
        Debug.Log("herpaderp");
        //this.gameObject.SetActive(false);
        ReturnChildPositions();
    }

    public void GetChildPositions()
    {
        pelvisPosition = pelvis.position;
        LhipsPosition = Lhips.position;
        LkneePosition = Lknee.position;
        LfootPosition = Lfoot.position;
        RhipsPosition = Rhips.position;
        RkneePosition = Rknee.position;
        RfootPosition = Rfoot.position;
        middleSpinePosition = middleSpine.position;
        headPosition = head.position;
        LupperarmPosition = Lupperarm.position;
        LforearmPosition = Lforearm.position;
        RupperarmPosition = Rupperarm.position;
        RforearmPosition = Rforearm.position;
    }

    public void ReturnChildPositions()
    {
        pelvis.position = pelvisPosition;
        Lhips.position = LhipsPosition;
        Lknee.position = LkneePosition;
        Lfoot.position = LfootPosition;
        Rhips.position = RhipsPosition;
        Rknee.position = RkneePosition;
        Rfoot.position = RfootPosition;
        middleSpine.position = middleSpinePosition;
        head.position = headPosition;
        Lupperarm.position = LupperarmPosition;
        Lforearm.position = LforearmPosition;
        Rupperarm.position = RupperarmPosition;
        Rforearm.position = RforearmPosition;
    }
}
