﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Health))]
public class Unit : MonoBehaviour, IUnit {

#region IUnit
    public IHealth Health { get { return health; } }
    public string Name { get { return unitName; } }
    public Player Owner { get { return owner; } }
    public IUnitController Controller { get { return controller; } }
    #endregion

    [SerializeField]
    private string unitName;
    private Player owner;
    private IHealth health;
    private IUnitController controller;

    public UnitAbilityHandler abilityHandler;
    public UnitAttributes attributes;
    public Inventory unitInventory;

    private void Awake()
    {
        owner = GetComponentInParent<Player>();
        controller = GetComponent<UnitController>();
        health = GetComponent<Health>();
    }

    private void OnLevelWasLoaded(int level)
    {
        unitInventory.ClearInventory();
        abilityHandler.ClearAbilityList();

        Health.Current = Health.Max;
    }
}
