﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooldown : MonoBehaviour {

    public float CoolDown { get { return coolDownTime; } set { coolDownTime = value; } }
    public bool IsOnCoolDown { get { return isOnCoolDown; } set { isOnCoolDown = value; } }

    [SerializeField]
    private float coolDownTime = 0.0f;
    private bool isOnCoolDown = false;

    private float currentTime;

    private void Update()
    {
        if (!IsOnCoolDown)
        {
            currentTime = 0.0f;
        }
        else
        {
            if (currentTime < CoolDown)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                Debug.Log("cooldown complete");
                IsOnCoolDown = false;
            }
        }
    }
}
