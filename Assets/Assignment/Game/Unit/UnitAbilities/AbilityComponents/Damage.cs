﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour, IDamageSource {

    [SerializeField]
    private DamageType damageType;

    public DamageType DamageType { get { return damageType; }  set { damageType = value; } }

    public void ApplyDamage(IHealth i_health, float i_damageAmount)
    {
        DamageInfo damageInfo = new DamageInfo(this, i_damageAmount, damageType);
        i_health.Damage(damageInfo);
    }
}
