﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AIDirectionalTargeting : DirectionTargeting
{

    public override void UpdateTargetDirection()
    {
        Direction = controller.TargetDirection();
    }

    private void Awake()
    {
        controller = GetComponentInParent<AIPlayerController>();
        owner = GetComponentInParent<Unit>();
    }

    // Use this for initialization
    private void Start()
    {
        OnDirectionTargeted = new DirectionEventCallback();
        OnTargetingCancelled = new UnityEvent();
    }

    // FixedUpdate is called once per framerate frame
    void FixedUpdate()
    {
        if (IsTargeting)
        {
            UpdateTargetDirection();

            if (OnDirectionTargeted != null)
                OnDirectionTargeted.Invoke(Direction);
        }
    }
}
