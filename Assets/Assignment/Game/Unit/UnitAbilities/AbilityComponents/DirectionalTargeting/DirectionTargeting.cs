﻿using UnityEngine;
using UnityEngine.Events;
using System;

public class DirectionTargeting : MonoBehaviour {

    public class DirectionEventCallback : UnityEvent<Vector3> { }
    public DirectionEventCallback OnDirectionTargeted = null;

    [NonSerialized]
    public UnityEvent OnTargetingCancelled = null;

    [NonSerialized]
    public PlayerController controller = null;

    [NonSerialized]
    public Unit owner = null;

    public bool IsTargeting
    {
        get { return isTargeting; }
        set
        {
            isTargeting = value;

            if (!IsTargeting)
            {
                OnTargetingCancelled.Invoke();
                controller.ResetMovementControl();
            }
            else
            {
                controller.RemoveMovementControl();
            }
        }
    }

    virtual public void UpdateTargetDirection() { }

    public void ResetTargeting()
    {
        IsTargeting = false;
    }

    public Vector3 Direction { get { return direction; } set { direction = value; } }

    private Vector3 direction;

    private bool isTargeting = false;
}
