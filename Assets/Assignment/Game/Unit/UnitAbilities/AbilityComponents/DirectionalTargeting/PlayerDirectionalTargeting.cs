﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerDirectionalTargeting : DirectionTargeting {

    public override void UpdateTargetDirection()
    {
        raycast = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast (raycast.origin, raycast.direction, out raycastHit))
        {
            Vector3 position = raycastHit.point;

            Direction = (position - owner.transform.position).normalized;

            Direction -= new Vector3(0.0f, Direction.y, 0.0f);
        }
    }

    private void Awake()
    {
        controller = GetComponentInParent<LocalPlayerController>();
        owner = GetComponentInParent<Unit>();
    }

    // Use this for initialization
    private void Start ()
    {
        OnDirectionTargeted = new DirectionEventCallback();
        OnTargetingCancelled = new UnityEvent();
    }

	// FixedUpdate is called once per framerate frame
	void FixedUpdate ()
    {
        if (IsTargeting)
        {
            UpdateTargetDirection();

            if (OnDirectionTargeted != null)
                OnDirectionTargeted.Invoke(Direction);
        }
	}

    private Ray raycast;
    private RaycastHit raycastHit;
}
