﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorProjectile : MonoBehaviour {

    public void SetProperties(Meteor i_ability, float i_fallTime, float i_impactForce, float i_impactRange, float i_impactDamage,
                                                float i_rollDamage, float i_rollSpeed, float i_rollDuration)
    {
        ability = i_ability;
        fallTime = i_fallTime;
        impactForce = i_impactForce;
        impactRange = i_impactRange;
        impactDamage = i_impactDamage;
        rollDamage = i_rollDamage;
        rollSpeed = i_rollSpeed;
        rollDuration = i_rollDuration;

        hasImpacted = false;
    }

    public void Launch(Vector3 i_direction, Vector3 i_position)
    {
        direction = i_direction;

        Vector3 spawnPosition = 15 * i_direction + i_position;
        spawnPosition += new Vector3(0.0f, 15.0f, 0.0f);

        Activate(spawnPosition);

        Vector3 impactPoint = GetImpactPoint(spawnPosition);

        StopAllCoroutines();
        StartCoroutine(DropMeteor(impactPoint));
    }

    private void Activate(Vector3 i_position)
    {
        transform.position = i_position;
    }

    private void ReturnToPool()
    {
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        // Return to object pool
        FireballObjectPool.current.Return(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Walkable" && !hasImpacted)
        {
            hasImpacted = true;
            Debug.Log("Impact!");
            StopAllCoroutines();

            DamageAllWithinRange();

            StartCoroutine(RollMeteor());
        }

        if (hasImpacted)
        {
            Unit detectedUnit = other.GetComponent<Unit>();
            Health attachedHealth = other.GetComponent<Health>();

            if (attachedHealth != null)
            {
                if (attachedHealth.IsInvincible)
                    return;
            }

            if (attachedHealth != null && detectedUnit != ability.Owner)
            {
                Debug.Log("Roll Damage!");
                damage.ApplyDamage(attachedHealth, rollDamage);
                //if (KillAttributionEventBus.Instance.onDamageEvent != null)
                //    KillAttributionEventBus.Instance.onDamageEvent.Invoke(ability.Owner.Owner, detectedUnit);
            }
        }
    }

    void DamageAllWithinRange()
    {
        List<Collider> hitColliders = new List<Collider>(Physics.OverlapSphere(transform.position, impactRange));

        foreach (Collider collider in hitColliders)
        {
            Unit detectedUnit = collider.GetComponent<Unit>();
            Health attachedHealth = collider.GetComponent<Health>();
            UnitController unitController = collider.GetComponent<UnitController>();

            if (attachedHealth != null && unitController != null && detectedUnit != ability.Owner)
            {
                if (attachedHealth.IsInvincible)
                    continue;

                damage.ApplyDamage(attachedHealth, impactDamage);

                //if (KillAttributionEventBus.Instance.onDamageEvent != null)
                //    KillAttributionEventBus.Instance.onDamageEvent.Invoke(ability.Owner.Owner, detectedUnit);

                unitController.ApplyForce(transform.position, impactForce);
                unitController.Interrupt();
            }   
        }
    }

    private Vector3 GetImpactPoint(Vector3 i_StartPoint)
    {
        RaycastHit[] hitObjects;

        hitObjects = Physics.RaycastAll(i_StartPoint, Vector3.down);

        foreach (RaycastHit hit in hitObjects)
        {
            if (hit.transform.tag == "Walkable")
            {
                return hit.point;
            }
        }

        Debug.Log("Ground not found?!");

        return new Vector3();
    }

    IEnumerator DropMeteor(Vector3 i_impactPoint)
    {
        float currentTime = 0.0f;

        Vector3 currentPosition = transform.GetComponent<Rigidbody>().position;

        while (currentTime < 1)
        {
            currentTime += Time.deltaTime / fallTime;

            transform.GetComponent<Rigidbody>().position = Vector3.Lerp(currentPosition, i_impactPoint, currentTime);

            yield return null;
        }

        yield break;
    }

    IEnumerator RollMeteor()
    {
        float currentTime = 0.0f;

        while (currentTime <= rollDuration)
        {
            transform.Translate(direction * rollSpeed * Time.deltaTime);

            currentTime += Time.deltaTime;

            StickToGround();

            yield return null;
        }

        ReturnToPool();

        yield break;
    }

    void StickToGround()
    {
        RaycastHit hit;

        Physics.Raycast(transform.position, Vector3.down, out hit);

        float radius = GetComponent<SphereCollider>().radius;

        if (hit.distance > radius)
        {
            transform.position += new Vector3(0.0f, (radius - hit.distance) + radius, 0.0f);
        }
    }

    [SerializeField]
    private Damage damage;

    private Meteor ability;

    private bool hasImpacted;

    private Vector3 direction;

    #region Fall and Impact

    float fallTime;

    float impactForce;

    float impactRange;

    float impactDamage;

    #endregion

    #region Roll

    float rollDamage;

    float rollSpeed;

    float rollDuration;

    #endregion
}
