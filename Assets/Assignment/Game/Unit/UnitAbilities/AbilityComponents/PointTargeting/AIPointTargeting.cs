﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AIPointTargeting : PointTargeting
{
    public override bool CheckForValidInput()
    {
        Location = controller.TargetPosition();

        return true;
    }

    private void Awake()
    {
        controller = GetComponentInParent<AIPlayerController>();
    }

    private void Start()
    {
        OnPointTargeted = new PointEventCallback();
        OnTargetingCancelled = new UnityEvent();
    }
}
