﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerPointTargeting : PointTargeting {

	public override bool CheckForValidInput()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(clickRay.origin, clickRay.direction, out clickHit))
            {
                OnPointTargeted.Invoke(clickHit.point);

                if (clickHit.collider.tag == "Walkable")
                {
                    Location = clickHit.point;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void Awake()
    {
        controller = GetComponentInParent<LocalPlayerController>();
    }

    private void Start()
    {
        OnPointTargeted = new PointEventCallback();
        OnTargetingCancelled = new UnityEvent();
    }

    private Ray clickRay;
    private RaycastHit clickHit;
}
