﻿using UnityEngine;
using UnityEngine.Events;
using System;

public class PointTargeting : MonoBehaviour {

    public class PointEventCallback : UnityEvent<Vector3> { }
    [NonSerialized]
    public PointEventCallback OnPointTargeted = null;

    [NonSerialized]
    public UnityEvent OnTargetingCancelled = null;

    [NonSerialized]
    public PlayerController controller = null;

    [NonSerialized]
    public Unit owner = null;

    public bool IsTargeting { get { return isTargeting; }
        set
        {
            isTargeting = value;

            if (!IsTargeting)
            {
                OnTargetingCancelled.Invoke();
                controller.ResetMovementControl();
            }
            else
            {
                controller.RemoveMovementControl();
            }
        }
    }

    public Vector3 Location { get { return location; } set { location = value; } }

    virtual public bool CheckForValidInput() { return true; }

    public void ResetTargeting()
    {
        IsTargeting = false;
    }

    private Vector3 location;

    private bool isTargeting = false;
}
