﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviour
{
    public Vector3 Direction { get { return direction; } set { direction = value; } }

    public Fireball Ability { get { return ability; } }

    public void SetProperties(Fireball i_ability, float i_speed, float i_duration, float i_size, float i_collisionDamage, float i_collisionForce)
    {
        ability = i_ability;
        speed = i_speed;
        duration = i_duration;
        size = i_size;
        collisionDamage = i_collisionDamage;
        collisionForce = i_collisionForce;
    }

    public void Launch(Vector3 i_direction, Vector3 i_position)
    {
        Activate(i_position);

        Direction = i_direction;

        StopAllCoroutines();
        StartCoroutine(MoveProjectile());
    }

    IEnumerator MoveProjectile()
    {
        float currentTime = 0.0f;

        while (currentTime <= duration)
        {
            transform.Translate(Direction * speed * Time.deltaTime);

            currentTime += Time.deltaTime;
            yield return null;
        }

        ReturnToPool();

        yield break;
    }

    private void Activate(Vector3 i_position)
    {
        transform.position = i_position;
        transform.localScale *= size;
    }

    private void ReturnToPool()
    {
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        // Return to object pool
        FireballObjectPool.current.Return(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Unit detectedUnit = other.gameObject.GetComponent<Unit>();
        Health attachedHealth = other.gameObject.GetComponent<Health>();
        UnitController unitController = other.gameObject.GetComponent<UnitController>();

        if (attachedHealth != null)
        {
            if (attachedHealth.IsInvincible)
                return;
        }

        if (attachedHealth != null && unitController != null && detectedUnit != ability.Owner)
        {
            unitController.ApplyForce(ability.Owner.transform.position, collisionForce);
            damage.ApplyDamage(attachedHealth, collisionDamage);

            // Set this plyer as the affected units damage attributer
            //if (KillAttributionEventBus.Instance.onDamageEvent != null)
            //    KillAttributionEventBus.Instance.onDamageEvent.Invoke(ability.Owner.Owner, detectedUnit);

            unitController.Interrupt();
            ReturnToPool();
		}
    }

    [SerializeField]
    private Damage damage;

    private Fireball ability;

    private float speed;
    private float duration;
    private float size;
    private float collisionDamage;
    private float collisionForce;

    private Vector3 direction;
}
