﻿using System;
using UnityEngine;

public class SkillCast : MonoBehaviour {

    public float CastTime { get { return castTime; } set { castTime = value; } }

    [SerializeField]
    private float castTime = 1.5f;

    [NonSerialized]
    public SkillCastHandler castHandler;

    [SerializeField]
    private float animationTime;

    private Animator unitAnimator;

    public void ClearCallback()
    {
        castHandler.OnSkillCast.RemoveAllListeners();
    }

    public void StartAnimation(float i_castTime)
    {
        unitAnimator.SetTrigger("IsChanneling");

        unitAnimator.speed = animationTime / i_castTime;
    }

    private void Start()
    {
        castHandler = GetComponentInParent<SkillCastHandler>();
        unitAnimator = GetComponentInParent<Animator>();
    }
}
