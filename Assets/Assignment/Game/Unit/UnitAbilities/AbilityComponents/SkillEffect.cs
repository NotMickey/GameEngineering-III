﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SkillEffect : MonoBehaviourPunCallbacks
{
    [SerializeField]
    GameObject skillEffect;

    [SerializeField]
    Ability owningAbility;

    GameObject effect;

	public void PlayEffect()
    {
        if(skillEffect)
        {
            effect = Instantiate(skillEffect, transform);
            effect.transform.position = owningAbility.Owner.transform.position;

            effect.GetComponent<ParticleSystem>().Play();

            StopAllCoroutines();
            StartCoroutine(PlayAndStop(3.0f));
        }
    }

    IEnumerator PlayAndStop(float i_time)
    {
        yield return new WaitForSeconds(i_time);

        StopEffect();

        yield break;
    }

    public void StopEffect()
    {
        if (effect)
        {
            effect.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            Destroy(effect);
            effect = null;
        }
    }
}
