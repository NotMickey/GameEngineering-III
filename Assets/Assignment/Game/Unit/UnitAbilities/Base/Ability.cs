﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour, IAbility
{
	public string AbilityName { get { return name; } protected set { name = value; } }

	public int AbilityLevel { get { return abilityLevel; } }

	public bool CanExecute { get { return canExecute; } set { canExecute = value; } }

	public Unit Owner { get { return owner; } set { owner = value; } }

	public string Description { get { return abilityDescription; } }

	public virtual void Execute()
	{
		if (owner == null)
		{
			Debug.Log("Ability : " + abilityName + " has no owner!");
			canExecute = false;
		}
		else
			canExecute = true;


		if (!Owner.Owner.GetComponent<PlayerController>().IsControllable)
		{
			Debug.Log("Can't control player yet!");
			canExecute = false;
		}
		else
			canExecute = true;

		ModifyAttributes();
	}

	public virtual void ModifyAttributes() { }

	[SerializeField]
	private string abilityName = "default";

	[SerializeField]
	private int abilityLevel = 0;

	[SerializeField]
	private string abilityDescription = "Description goes here";

	private bool canExecute = true;

	private Unit owner = null;
}
