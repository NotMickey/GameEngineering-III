﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAbility {

    string AbilityName { get; }
    int AbilityLevel { get; }
    string Description { get; }
    bool CanExecute { get; set; }
    Unit Owner { get; set; }

    void Execute();
    void ModifyAttributes();
}
