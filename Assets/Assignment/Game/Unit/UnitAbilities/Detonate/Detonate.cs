﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Detonate : Ability, IDamageSource{

    public Detonate(Unit owner)
    {
        Owner = owner;
    }

    [SerializeField]
    private float innerRadius;

    [SerializeField]
    private float outerRadius;

    [SerializeField]
    private float innerDamage;

    [SerializeField]
    private float outerDamage;

    [SerializeField]
    private float innerForce;

    [SerializeField]
    private float outerForce;

    [SerializeField]
    private PhotonView unitView;

    public override void Execute()
    {
        if (!unitView.IsMine)
            return;

        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            cooldown.IsOnCoolDown = true;

            cast.castHandler.OnSkillCast.AddListener(DetonateAllWithinRange);
            //cast.castHandler.OnSkillCast.AddListener(effect.PlayEffect);

            Owner.Owner.GetComponent<PlayerController>().RemoveMovementControl();

            cast.StartAnimation(cast.CastTime);
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        innerRadius = Owner.attributes.ModifyAbilityRange(innerRadius);
        outerRadius = Owner.attributes.ModifyAbilityRange(outerRadius);

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    void DetonateAllWithinRange()
    {
        if (unitView)
        {
            unitView.RPC("DetonateAbilityNetwork", RpcTarget.All, unitView.ViewID);
        }

        cast.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);

        Owner.Owner.GetComponent<PlayerController>().ResetMovementControl();
    }

    public void DetonateAllOverNetwork()
    {
        effect.PlayEffect();

        List<Collider> hitCollidersOuter = new List<Collider>(Physics.OverlapSphere(Owner.transform.position, outerRadius));

        List<Collider> hitCollidersInner = new List<Collider>(Physics.OverlapSphere(Owner.transform.position, innerRadius));

        // Remove all common units from outer list
        foreach (Collider collider in hitCollidersInner)
        {
            hitCollidersOuter.Remove(collider);
        }

        //Apply inner force and damage
        ExplodeAllUnits(hitCollidersInner, innerDamage, innerForce);

        //Apply outer force and damage
        ExplodeAllUnits(hitCollidersOuter, outerDamage, outerForce);
    }

    void ExplodeAllUnits(List<Collider> i_unitColliders, float i_damage, float i_force)
    {
        foreach (Collider collider in i_unitColliders)
        {
            Unit detectedUnit = collider.GetComponent<Unit>();
            Health attachedHealth = collider.GetComponent<Health>();
            UnitController unitController = collider.GetComponent<UnitController>();

            if (attachedHealth != null)
            {
                if (attachedHealth.IsInvincible)
                    continue;

                //Debug.Log("Applying damage to : " + detectedUnit.Owner.Name);

                damage.ApplyDamage(attachedHealth, i_damage);
                //if (KillAttributionEventBus.Instance.onDamageEvent != null)
                //    KillAttributionEventBus.Instance.onDamageEvent.Invoke(Owner.Owner, detectedUnit);
            }

            if (unitController != null && detectedUnit != Owner)
            {
                unitController.Interrupt();

                unitController.ApplyForce(Owner.transform.position, i_force);
            }    
        }
    }

    private void Start()
    {
        AbilityName = "Detonate";
        unitView = GetComponentInParent<PhotonView>();
    }

    [SerializeField]
    private Cooldown cooldown;
    [SerializeField]
    private Damage damage;
    [SerializeField]
    private SkillCast cast;
    [SerializeField]
    private SkillEffect effect;
}
