﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Fireball : Ability {

    [SerializeField]
    private float projectileSpeed;
    [SerializeField]
    private float projectileDuration;
    [SerializeField]
    private float projectileSize;
    [SerializeField]
    private float collisionDamage;
    [SerializeField]
    private float collisionForce;

    [SerializeField]
    private PhotonView unitView;

    Fireball(Unit owner)
    {
        Owner = owner;
    }

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            if (targeting.IsTargeting)
            {

            }
            else
            {
                targeting.IsTargeting = true;

                // set up callbacks
                targeting.OnDirectionTargeted.AddListener(UpdatePlayerDirection);
                targeting.controller.OnClickedCallback.AddListener(CastSkill);

                cast.castHandler.OnSkillInterrupted.AddListener(targeting.ResetTargeting);

            }
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        projectileDuration = Owner.attributes.ModifyAbilityRange(projectileDuration);
        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void CastSkill()
    {
        if (!cooldown.IsOnCoolDown)
        {
            cast.castHandler.OnSkillCast.AddListener(LaunchProjectile);
            cast.StartAnimation(cast.CastTime);

            //put ability on cooldown
            cooldown.IsOnCoolDown = true;
            
            targeting.OnDirectionTargeted.RemoveAllListeners();
        }  
    }

    private void LaunchProjectile()
    {
        //GameObject fireball = FireballObjectPool.current.Get();

        //fireball.GetComponent<Projectile>().SetProperties(this, projectileSpeed, projectileDuration, projectileSize, collisionDamage, collisionForce);
        //fireball.GetComponent<Projectile>().Launch(Owner.transform.forward, Owner.transform.position + new Vector3(0.0f, 2.0f, 0.0f));

        if (unitView)
        {
            unitView.RPC("FireballAbilityNetwork", RpcTarget.All, unitView.ViewID);
        }

        targeting.IsTargeting = false;
        cast.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);
    }

    public void LaunchFireballNetwork()
    {
        GameObject fireball = FireballObjectPool.current.Get();

        fireball.GetComponent<Projectile>().SetProperties(this, projectileSpeed, projectileDuration, projectileSize, collisionDamage, collisionForce);
        fireball.GetComponent<Projectile>().Launch(Owner.transform.forward, Owner.transform.position + new Vector3(0.0f, 2.0f, 0.0f));
    }

    private void UpdatePlayerDirection(Vector3 i_direction)
    {
        Owner.transform.rotation = Quaternion.LookRotation(i_direction, Vector3.up);
    }

    private void Start()
    {
        AbilityName = "Fireball";
        unitView = GetComponentInParent<PhotonView>();
    }

    [SerializeField]
    DirectionTargeting targeting;

    [SerializeField]
    Cooldown cooldown;

    [SerializeField]
    SkillCast cast;
}
