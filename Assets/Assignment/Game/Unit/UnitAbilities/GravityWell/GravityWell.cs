﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GravityWell : Ability {

    [SerializeField]
    GameObject gravityWellPrefab;

    [SerializeField]
    private PhotonView unitView;

    #region Casting

    [SerializeField]
    private Cooldown cooldown;
    [SerializeField]
    private PointTargeting pointTargeting;
    [SerializeField]
    private SkillCast castAnimation;
    [SerializeField]
    private float castRange;

    private Vector3 wellLocation;

    #endregion

    #region Setup

    [SerializeField]
    private float setupTime = 1.0f;

    #endregion

    #region Active

    [SerializeField]
    private float duration;

    [SerializeField]
    private float innerRadius;

    [SerializeField]
    private float innerForce;

    [SerializeField]
    private float outerRadius;

    [SerializeField]
    private float outerForce;

    // Quadratic force curve?

    #endregion

    private bool isWellActive = false;
    
    GravityWell(Unit owner)
    {
        Owner = owner;
    }

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            if (pointTargeting.IsTargeting)
            {
                // Do nothing!
            }
            else
            {
                pointTargeting.IsTargeting = true;

                // set up callbacks
                pointTargeting.controller.OnClickedCallback.AddListener(ActivateWell);
                castAnimation.castHandler.OnSkillInterrupted.AddListener(pointTargeting.ResetTargeting);
            }
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        innerRadius = Owner.attributes.ModifyAbilityRange(innerRadius);
        outerRadius = Owner.attributes.ModifyAbilityRange(outerRadius);

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void ActivateWell()
    {
        if (pointTargeting.CheckForValidInput())
        {
            wellLocation = CalculateTargetLocation(pointTargeting.Location);

            if (wellLocation.magnitude == 0.0f)
            {
                // location out of range!
                Debug.Log("OUT OF RANGE!");

                return;
            }
            else
            {
                if (!cooldown.IsOnCoolDown)
                {
                    castAnimation.castHandler.OnSkillCast.AddListener(CastSkill);
                    castAnimation.StartAnimation(castAnimation.CastTime);

                    //make player look towards point
                    Vector3 targetDir = (wellLocation - Owner.transform.position).normalized;

                    Owner.transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);

                    //put ability on cooldown
                    cooldown.IsOnCoolDown = true;
                }
            }
        }
        else
        {
            // clicked invalid terrain
            Debug.Log("invalid location!");
            return;
        }
    }

    private void CastSkill()
    {
        unitView.RPC("GravityWellAbilityNetwork", RpcTarget.All, wellLocation, unitView.ViewID);

        // After ability is cast reset callbacks
        pointTargeting.IsTargeting = false;
        castAnimation.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);
    }

    public void SpawnWellNetworked(Vector3 wellLocation)
    {
        GameObject well = Instantiate(gravityWellPrefab);

        well.transform.position = wellLocation + new Vector3(0.0f, 2.0f, 0.0f);
        well.GetComponent<SpawnedWell>().SetProperties(this, setupTime, duration, innerRadius, innerForce, outerRadius, outerForce);
    }

    private void Start()
    {
        AbilityName = "GravityWell";
        unitView = GetComponentInParent<PhotonView>();
    }

    private Vector3 CalculateTargetLocation(Vector3 i_targetedLocation)
    {
        float distance;

        // Check whether point is within range
        distance = (i_targetedLocation - Owner.transform.position).magnitude;

        Debug.Log(distance);

        if (distance > castRange)
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
        else
        {
            return i_targetedLocation;
        }
    }
}
