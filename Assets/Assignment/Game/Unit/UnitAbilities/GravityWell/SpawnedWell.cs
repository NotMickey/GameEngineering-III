﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnedWell : MonoBehaviour
{
    [SerializeField]
    private GameObject effectPrefab;

    private GameObject effect;

    public void SetProperties(Ability i_ability, float i_setupTime, float i_duration, float i_innerRadius,
                              float i_innerForce, float i_outerRadius, float i_outerForce)
    {
        ability = i_ability;
        setupTime = i_setupTime;
        duration = i_duration;
        innerRadius = i_innerRadius;
        innerForce = i_innerForce;
        outerRadius = i_outerRadius;
        outerForce = i_outerForce;
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Setup());

        if (effectPrefab != null)
        {
            effect = Instantiate(effectPrefab);
            effect.transform.position = transform.position;

            effect.GetComponent<ParticleSystem>().Play();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isActive)
        {
            PullAllWithinRange();
        }
	}

    void PullAllWithinRange()
    {
        List<Collider> hitCollidersOuter = new List<Collider>(Physics.OverlapSphere(transform.position, outerRadius));

        List<Collider> hitCollidersInner = new List<Collider>(Physics.OverlapSphere(transform.position, innerRadius));

        // Remove all common units from outer list
        foreach (Collider collider in hitCollidersInner)
        {
            hitCollidersOuter.Remove(collider);
        }

        //Apply inner force and damage
        PullAllUnits(hitCollidersInner, innerForce);

        //Apply outer force and damage
        PullAllUnits(hitCollidersOuter, outerForce);
    }

    void PullAllUnits(List<Collider> i_unitColliders, float i_force)
    {
        foreach (Collider collider in i_unitColliders)
        {
            Unit detectedUnit = collider.GetComponent<Unit>();
            UnitController unitController = collider.GetComponent<UnitController>();
            Health attachedHealth = collider.GetComponent<Health>();

            if (attachedHealth != null)
            {
                if (attachedHealth.IsInvincible)
                    continue;
            }

            if (detectedUnit != null)
            {
                //if (KillAttributionEventBus.Instance.onDamageEvent != null)
                //    KillAttributionEventBus.Instance.onDamageEvent.Invoke(ability.Owner.Owner, detectedUnit);

                //Debug.Log("pulling" + detectedUnit);
            }

            if (unitController != null)
            {
                Vector3 direction = (transform.position - detectedUnit.transform.position).normalized;

                unitController.rb.position += direction * i_force * Time.deltaTime;
            }
        }
    }

    IEnumerator Setup()
    {
        yield return new WaitForSeconds(setupTime);

        StartCoroutine(DestroyAfterTime());
        effect.transform.localScale = new Vector3(7.2f, 7.2f, 7.2f);
        isActive = true;
    }

    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(duration);

        effect.GetComponent<ParticleSystem>().Stop();
        isActive = false;

        Destroy(this);
    }

    private bool isActive = false;
    private float time = 0.0f;

    private Ability ability;
    private float setupTime = 1.0f;
    private float duration;
    private float innerRadius;
    private float innerForce;
    private float outerRadius;
    private float outerForce;

}
