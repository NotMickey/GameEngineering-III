﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisibility : Ability {

    public Invisibility(Unit owner)
    {
        Owner = owner;
    }

    [SerializeField]
    private float duration;

    [SerializeField]
    private float castTime;

    private Renderer unitRenderer;

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            cooldown.IsOnCoolDown = true;

            BecomeInvisible();

            //Owner.Owner.GetComponent<PlayerController>().RemoveMovementControl();
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void BecomeInvisible()
    {
        StopAllCoroutines();
        StartCoroutine(TurnTransparent());

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);

        SkillCastEventBus.Instance.OnSkillCast.AddListener(InvisibilityCancelled);
    }

    private void BecomeVisible()
    {
        StopAllCoroutines();

        Color color = unitRenderer.material.color;
        color.a = 1.0f;
        unitRenderer.material.color = color;

        SkillCastEventBus.Instance.OnSkillCast.RemoveListener(InvisibilityCancelled);
    }

    private void InvisibilityCancelled(Unit owner, Ability castAbility)
    {
        if (owner == Owner && castAbility != this)
        {
            BecomeVisible();
        }
    }

    IEnumerator TurnTransparent()
    {
        float time = 0.0f;

        Color color = unitRenderer.material.color;

        while (time <= castTime)
        {
            time += Time.deltaTime;
            
            color.a = 1 - time/castTime;
            
            if (time >= castTime)
            {
                color.a = 0.1f;
            }
            unitRenderer.material.color = color;

            yield return null;
        }

        StartCoroutine(InvisibilityDuration());

        yield break;
    }

    IEnumerator InvisibilityDuration()
    {
        yield return new WaitForSeconds(duration);

        BecomeVisible();

        yield break;
    }

    private void Start()
    {
        unitRenderer = transform.parent.transform.parent.GetComponentInChildren<Renderer>();
        AbilityName = "Invisibility";
    }

    [SerializeField]
    private Cooldown cooldown;
}
