﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Meteor : Ability {

    #region Fall and Impact

    [SerializeField]
    float fallTime;

    [SerializeField]
    float impactForce;

    [SerializeField]
    float impactRange;

    [SerializeField]
    float impactDamage;

    #endregion

    #region Roll

    [SerializeField]
    float rollDamage;

    [SerializeField]
    float rollSpeed;

    [SerializeField]
    float rollDuration;

    #endregion

    #region Casting

    [SerializeField]
    DirectionTargeting targeting;

    [SerializeField]
    Cooldown cooldown;

    [SerializeField]
    SkillCast cast;

    #endregion

    [SerializeField]
    private PhotonView unitView;

    Meteor(Unit owner)
    {
        Owner = owner;
    }

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            if (targeting.IsTargeting)
            {

            }
            else
            {
                targeting.IsTargeting = true;

                // set up callbacks
                targeting.OnDirectionTargeted.AddListener(UpdatePlayerDirection);
                targeting.controller.OnClickedCallback.AddListener(CastSkill);

                cast.castHandler.OnSkillInterrupted.AddListener(targeting.ResetTargeting);
            }
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        impactRange = Owner.attributes.ModifyAbilityRange(impactRange);

        rollDuration = Owner.attributes.ModifyAbilityRange(rollDuration);

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void CastSkill()
    {
        if (!cooldown.IsOnCoolDown)
        {
            cast.castHandler.OnSkillCast.AddListener(LaunchMeteor);
            cast.StartAnimation(cast.CastTime);

            //put ability on cooldown
            cooldown.IsOnCoolDown = true;

            targeting.OnDirectionTargeted.RemoveAllListeners();
        }
    }

    private void LaunchMeteor()
    {
        unitView.RPC("MeteorAbilityNetwork", RpcTarget.All, unitView.ViewID);

        targeting.IsTargeting = false;
        cast.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);
    }

    public void LaunchFireballNetwork()
    {
        GameObject meteor = MeteorObjectPool.current.Get();

        meteor.GetComponent<MeteorProjectile>().SetProperties(this, fallTime, impactForce, impactRange, impactDamage, rollDamage, rollSpeed, rollDuration);
        meteor.GetComponent<MeteorProjectile>().Launch(Owner.transform.forward, Owner.transform.position + new Vector3(0.0f, 2.0f, 0.0f));
    }

    private void UpdatePlayerDirection(Vector3 i_direction)
    {
        Owner.transform.rotation = Quaternion.LookRotation(i_direction, Vector3.up);
    }

    private void Start()
    {
        AbilityName = "Meteor";
        unitView = GetComponentInParent<PhotonView>();
    }
}
