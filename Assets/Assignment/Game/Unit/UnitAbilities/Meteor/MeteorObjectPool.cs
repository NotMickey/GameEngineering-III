﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MeteorObjectPool : UnityObjectPool<GameObject>
{
	public static MeteorObjectPool current;

	public override bool AutoFill { get { return autoFill; } }

	public override int MinSize { get { return minSize; } }

	public override bool CreateIfNecessary { get { return createIfNecessary; } }

	public override GameObject Get()
	{
		for (int i = 0; i < objectPool.Count; ++i)
		{
			if (!objectPool[i].activeInHierarchy)
			{
				objectPool[i].SetActive(true);

				Debug.Log("Object removed from pool...new count = " + GetCount());

				return objectPool[i];
			}
		}

		if (CreateIfNecessary)
		{
			GameObject obj = Instantiate(prefab);
			objectPool.Add(obj);

			return obj;
		}

		return null;
	}

	public override List<GameObject> Get(int count)
	{
		List<GameObject> returnList = new List<GameObject>();

		for (int i = 0; i < count; ++i)
		{
			if (objectPool.Count == 0)
			{
				if (CreateIfNecessary)
				{
					returnList.Add(Instantiate(prefab));
				}
				else
				{
					return returnList;
				}
			}

			returnList.Add(objectPool[objectPool.Count - 1]);
			objectPool.RemoveAt(objectPool.Count - 1);
		}

		return returnList;
	}

	public override void Resize(int count)
	{
		throw new System.NotImplementedException();
	}

	public override void Return(GameObject t)
	{
		for (int i = 0; i < objectPool.Count; ++i)
		{
			if (t == objectPool[i])
			{
				objectPool[i].SetActive(false);
			}
		}

		Debug.Log("Object returned to pool...new count = " + GetCount());
	}

	public override void Return(List<GameObject> ts)
	{
		foreach (GameObject meteorProjectile in ts)
		{
			objectPool.Add(meteorProjectile);
		}

		ts.Clear();
	}

	public int GetCount()
	{
		int count = 0;

		for (int i = 0; i < objectPool.Count; ++i)
		{
			if (!objectPool[i].activeInHierarchy)
				count++;
		}

		return count;
	}

	[SerializeField]
	private bool autoFill;

	[SerializeField]
	private int minSize;

	[SerializeField]
	private bool createIfNecessary;

	private List<GameObject> objectPool;

	private void Awake()
	{
		if (current == null)
			current = this;
		else
			Destroy(this);

		objectPool = new List<GameObject>();
	}

	private void Start()
	{
		if (AutoFill)
		{
			for (int i = 0; i < minSize; ++i)
			{
				GameObject obj = Instantiate(prefab);
			obj.SetActive(false);

				objectPool.Add(obj);
			}
		}
	}
}
