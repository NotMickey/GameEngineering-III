﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillCastEventBus : MonoBehaviour {

    public static SkillCastEventBus Instance
    {
        get
        {
            return _instance;
        }
    }

    private static SkillCastEventBus _instance = null;

    [System.Serializable]
    public class SkillCastCallback : UnityEvent<Unit, Ability> { }
    [NonSerialized]
    public SkillCastCallback OnSkillCast = null;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }

        OnSkillCast = new SkillCastCallback();
    }

    private void OnDestroy()
    {
        OnSkillCast.RemoveAllListeners();
    }
}
