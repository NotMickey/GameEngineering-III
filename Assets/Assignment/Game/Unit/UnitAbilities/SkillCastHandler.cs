﻿using UnityEngine;
using UnityEngine.Events;
using System;

/// <summary>
/// This handles casting any skills once the attack animation reaches a certain point
/// </summary>
public class SkillCastHandler : MonoBehaviour {

    [NonSerialized]
    public UnityEvent OnSkillCast = null;

    [NonSerialized]
    public UnityEvent OnSkillInterrupted = null;

    [SerializeField]
    private Animator unitAnimator;

    public void SkillEvent()
    {
        OnSkillCast.Invoke();
        OnSkillInterrupted.RemoveAllListeners();

        unitAnimator.speed = 1;
    }

    public void SkillInterruptedEvent()
    {
        if (OnSkillInterrupted != null)
        {
            OnSkillInterrupted.Invoke();
            OnSkillInterrupted.RemoveAllListeners();
        }
            
        if (OnSkillCast != null)
        {
            OnSkillCast.RemoveAllListeners();
        }
            
        unitAnimator.speed = 1;
    }

    public void SkillCancelEvent()
    {
        //Debug.Log("Skill cancelled!");
        if (OnSkillInterrupted != null)
            OnSkillInterrupted.Invoke();

        OnSkillCast.RemoveAllListeners();
        OnSkillInterrupted.RemoveAllListeners();

        unitAnimator.speed = 1;
    }

    public void UnitDeathEvent()
    {
        Debug.Log("unit died!");
        OnSkillCast.RemoveAllListeners();
        OnSkillInterrupted.RemoveAllListeners();

        unitAnimator.speed = 1;
    }

    private void Start()
    {
        OnSkillCast = new UnityEvent();
        OnSkillInterrupted = new UnityEvent();
    }
}
