﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpellShield : Ability
{
    SpellShield (Unit owner)
    {
        Owner = owner;
    }

    #region Casting

    [SerializeField]
    private Cooldown cooldown;

    #endregion

    #region Active

    [SerializeField]
    float duration;

    [SerializeField]
    private GameObject effectPrefab;

    private GameObject effect;

    public bool IsActive { get { return isActive; } set { isActive = value; } }
    private bool isActive;

    #endregion

    [SerializeField]
    private PhotonView unitView;

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            cooldown.IsOnCoolDown = true;

            ActivateShield();
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    void ActivateShield()
    {
        unitView.RPC("SpellShieldAbilityNetwork", RpcTarget.All, unitView.ViewID);
    }

    public void ActivateShieldNetworked()
    {
        isActive = true;
        GetComponentInParent<Health>().IsInvincible = true;

        StopAllCoroutines();
        StartCoroutine(DeactivateShield());

        if (effectPrefab != null)
        {
            effect = Instantiate(effectPrefab, transform);
            effect.transform.position = transform.position;

            effect.GetComponent<ParticleSystem>().Play();
        }
    }

    IEnumerator DeactivateShield()
    {
        //Debug.Log("Coroutine started for " + duration + " seconds");

        yield return new WaitForSeconds(duration);

        isActive = false;
        GetComponentInParent<Health>().IsInvincible = false;
        Destroy(effect);

        yield break;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Projectile projectile = collision.transform.GetComponent<Projectile>();

        if (projectile == null || projectile.Ability.Owner == Owner)
            return;

        ContactPoint contact = collision.contacts[0];

        Vector3 reflectedDirection = Vector3.Reflect(projectile.Direction, contact.normal);

        projectile.Direction = reflectedDirection;
    }

    // Use this for initialization
    void Start ()
    {
        AbilityName = "SpellShield";
        unitView = GetComponentInParent<PhotonView>();
	}
}
