﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Teleport : Ability
{
    float MaxDistance { get { return maxDistance; } }

    [SerializeField]
    private float maxDistance = 20.0f;

    [SerializeField]
    private PhotonView unitView;

    public Teleport(Unit owner)
    {
        Owner = owner;
    }

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            if (pointTargeting.IsTargeting)
            {
                // Do nothing!
            }
            else
            {
                pointTargeting.IsTargeting = true;

                // set up callbacks
                pointTargeting.controller.OnClickedCallback.AddListener(TeleportToLocation);
                cast.castHandler.OnSkillInterrupted.AddListener(pointTargeting.ResetTargeting);

            }
        }   
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        maxDistance = Owner.attributes.ModifyAbilityRange(maxDistance);

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void TeleportToLocation()
    {
        if (pointTargeting.CheckForValidInput())
        {
            teleportLocation = CalculateTeleportLocation(pointTargeting.Location);

            if (teleportLocation.magnitude == 0.0f)
            {
                // location out of range!
                Debug.Log("OUT OF RANGE!");

                return;
            }
            else
            {
                if (!cooldown.IsOnCoolDown)
                {
                    cast.castHandler.OnSkillCast.AddListener(CastSkill);
                    //cast.castHandler.OnSkillCast.AddListener(effect.PlayEffect);
                    cast.StartAnimation(cast.CastTime);

                    //make player look towards point
                    Vector3 targetDir = (teleportLocation - Owner.transform.position).normalized;

                    Owner.transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);

                    //put ability on cooldown
                    cooldown.IsOnCoolDown = true;
                }
            }
        }
        else
        {
            // clicked invalid terrain
            Debug.Log("invalid location!");
            return;
        }
    }

    public void TeleportOverNetwork()
    {
        effect.PlayEffect();

        Owner.transform.position = teleportLocation + new Vector3(0.0f, 2.0f, 0.0f);
    }

    private void CastSkill()
    {
        //Owner.transform.position = teleportLocation + new Vector3(0.0f, 2.0f, 0.0f);

        unitView.RPC("TeleportAbilityNetwork", RpcTarget.All, unitView.ViewID);

        // After ability is cast reset callbacks
        pointTargeting.IsTargeting = false;
        cast.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);
    }

    private void Start()
    {
        AbilityName = "Teleport";
        unitView = GetComponentInParent<PhotonView>();
    }

    private Vector3 CalculateTeleportLocation(Vector3 i_targetedLocation)
    {
        float distance;

        // Check whether point is within teleport range
        distance = (i_targetedLocation - Owner.transform.position).magnitude;

        Debug.Log(distance);

        if (distance > maxDistance)
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
        else
        {
            return i_targetedLocation;
        }
    }

    private Vector3 teleportLocation;

    [SerializeField]
    private Cooldown cooldown;
    [SerializeField]
    private PointTargeting pointTargeting;
    [SerializeField]
    private SkillCast cast;
    [SerializeField]
    private SkillEffect effect;
}
