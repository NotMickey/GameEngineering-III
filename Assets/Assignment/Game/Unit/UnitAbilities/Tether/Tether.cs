﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Tether : Ability
{
    #region Casting

    [SerializeField]
    private Cooldown cooldown;
    [SerializeField]
    private PointTargeting pointTargeting;
    [SerializeField]
    private SkillCast castAnimation;

    [SerializeField]
    private float castRadius;

    [SerializeField]
    private PhotonView unitView;

    private Vector3 castTarget;

    #endregion

    #region Active

    [SerializeField]
    private float bindRadius;

    [SerializeField]
    private float maxTetherDistance;

    [SerializeField]
    private float duration;

    bool isActive;

    List<Unit> detectedUnits;

    #endregion

    Tether(Unit owner)
    {
        Owner = owner;
    }

    public override void Execute()
    {
        base.Execute();

        if (!CanExecute)
            return;

        if (cooldown.IsOnCoolDown)
        {
            Debug.Log("ON COOLDOWN!");
            return;
        }
        else
        {
            if (pointTargeting.IsTargeting)
            {
                // Do nothing!
            }
            else
            {
                pointTargeting.IsTargeting = true;

                // set up callbacks
                pointTargeting.controller.OnClickedCallback.AddListener(ActivateTether);
                castAnimation.castHandler.OnSkillInterrupted.AddListener(pointTargeting.ResetTargeting);
            }
        }
    }

    public override void ModifyAttributes()
    {
        base.ModifyAttributes();

        castRadius = Owner.attributes.ModifyAbilityRange(castRadius);
        bindRadius = Owner.attributes.ModifyAbilityRange(bindRadius);

        cooldown.CoolDown = Owner.attributes.ModifyCooldown(cooldown.CoolDown);
    }

    private void ActivateTether()
    {
        if (pointTargeting.CheckForValidInput())
        {
            castTarget = CalculateTargetLocation(pointTargeting.Location);

            if (castTarget.magnitude == 0.0f)
            {
                // location out of range!
                Debug.Log("OUT OF RANGE!");

                return;
            }
            else
            {
                if (!cooldown.IsOnCoolDown)
                {
                    castAnimation.castHandler.OnSkillCast.AddListener(CastSkill);
                    castAnimation.StartAnimation(castAnimation.CastTime);

                    //make player look towards point
                    Vector3 targetDir = (castTarget - Owner.transform.position).normalized;

                    Owner.transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);

                    //put ability on cooldown
                    cooldown.IsOnCoolDown = true;
                }
            }
        }
        else
        {
            // clicked invalid terrain
            Debug.Log("invalid location!");
            return;
        }
    }

    private void CastSkill()
    {
        unitView.RPC("TetherAbilityNetwork", RpcTarget.All, unitView.ViewID);

        // After ability is cast reset callbacks
        pointTargeting.IsTargeting = false;
        castAnimation.ClearCallback();

        if (SkillCastEventBus.Instance.OnSkillCast != null)
            SkillCastEventBus.Instance.OnSkillCast.Invoke(Owner, this);
    }

    public void CastThetherNetwork()
    {
        List<Collider> hitColliders = new List<Collider>(Physics.OverlapSphere(Owner.transform.position, bindRadius));

        int count = 0;

        detectedUnits.Clear();

        for (int i = 0; i < hitColliders.Count; i++)
        {
            Unit detectedUnit = hitColliders[i].GetComponent<Unit>();
            Health attachedHealth = hitColliders[i].GetComponent<Health>();
            UnitController unitController = hitColliders[i].GetComponent<UnitController>();

            if (attachedHealth != null)
            {
                if (attachedHealth.IsInvincible)
                    continue;
            }

            if (detectedUnit != null && attachedHealth != null)
            {
                count++;

                detectedUnits.Add(detectedUnit);
            }

            if (count == 2)
                break;
        }

        if (count != 2)
        {
            Debug.Log("Not enough units!");
            return;
        }

        isActive = true;
        StopAllCoroutines();
        StartCoroutine(Deactivate());
    }

    private Vector3 CalculateTargetLocation(Vector3 i_targetedLocation)
    {
        float distance;

        // Check whether point is within range
        distance = (i_targetedLocation - Owner.transform.position).magnitude;

        Debug.Log(distance);

        if (distance > castRadius)
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
        else
        {
            return i_targetedLocation;
        }
    }

    private void PullUnits()
    {
        UnitController controller1 = detectedUnits[0].GetComponent<UnitController>();
        UnitController controller2 = detectedUnits[1].GetComponent<UnitController>();

        Vector3 displacement = detectedUnits[0].transform.position - detectedUnits[1].transform.position;

        if (displacement.magnitude > maxTetherDistance)
        {
            Vector3 direction = (detectedUnits[1].transform.position - detectedUnits[0].transform.position).normalized;
            controller1.rb.position += direction * 300f * Time.deltaTime;

            direction = (detectedUnits[0].transform.position - detectedUnits[1].transform.position).normalized;
            controller2.rb.position += direction * 300f * Time.deltaTime;
        }
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(duration);

        isActive = false;
        // destroy particle effect here : TODO

        yield break;
    }

    private void Start()
    {
        AbilityName = "Tether";
        unitView = GetComponentInParent<PhotonView>();

        detectedUnits = new List<Unit>();
    }

    private void Update()
    {
        if (isActive)
        {
            PullUnits();
        }
    }
}
