﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilityList
{
    public List<GameObject> ability;
    public List<float> cost;
}

[System.Serializable]
public class ItemList
{
    public List<GameObject> item;
    public List<float> cost;
}

public class UpgradeSystem : MonoBehaviour {

    [SerializeField]
    List<AbilityList> abilityList;

    [SerializeField]
    List<ItemList> itemList;

    #region Ability helper functions

    /// <summary>
    /// Returns the name of the ability
    /// </summary>
    /// <param name="abilityNumber"></param>
    /// <returns></returns>
    public string GetAbilityName(int abilityNumber)
    {
        return abilityList[abilityNumber].ability[0].GetComponent<Ability>().AbilityName;
    }

    /// <summary>
    /// Returns the total number of abilites in the game
    /// </summary>
    /// <returns></returns>
    public int AbilityCount()
    {
        return abilityList.Count;
    }

    /// <summary>
    /// Returns the cost of upgrade for that ability at that level
    /// </summary>
    /// <param name="abilityNumber"></param>
    /// <param name="upgradeNumber"></param>
    /// <returns></returns>
    public int GetCostOfAbilityUpgrade(string abilityName, int abilityLevel)
    {
        if (abilityLevel > 3)
            return 0;

        foreach (AbilityList abilities in abilityList)
        {
            if (abilityName == abilities.ability[0].GetComponent<Ability>().AbilityName)
            {
                return (int)abilities.cost[abilityLevel - 1];
            }
        }

        return 0;
    }

    /// <summary>
    /// Returns prefab for the selcted ability and its level
    /// </summary>
    /// <param name="abilityName"></param>
    /// <param name="abilityLevel"></param>
    /// <returns></returns>
    public GameObject GetAbilityPrefab(string abilityName, int abilityLevel)
    {
        foreach (AbilityList abilities in abilityList)
        {
            if (abilityName == abilities.ability[0].GetComponent<Ability>().AbilityName)
            {
                return abilities.ability[abilityLevel - 1];
            }
        }

        return null;
    }

    public string GetAbilityDescription(string abilityName, int abilityLevel)
    {
        foreach (AbilityList abilities in abilityList)
        {
            if (abilityName == abilities.ability[0].GetComponent<Ability>().AbilityName)
            {
                if (abilityLevel == 3)
                    return abilities.ability[2].GetComponent<Ability>().Description;

                return abilities.ability[abilityLevel].GetComponent<Ability>().Description;
            }
        }

        return "description";
    }

    #endregion

    #region Item helper functions

    /// <summary>
    /// Returns the name of the item
    /// </summary>
    /// <param name="itemNumber"></param>
    /// <returns></returns>
    public string GetItemName(int itemNumber)
    {
        return itemList[itemNumber].item[0].GetComponent<Item>().ItemName;
    }

    /// <summary>
    /// Returns the total number of items in the game
    /// </summary>
    /// <returns></returns>
    public int ItemCount()
    {
        return itemList.Count;
    }

    /// <summary>
    /// Returns the cost of upgrade for that item at that level
    /// </summary>
    /// <param name="itemName"></param>
    /// <param name="itemLevel"></param>
    /// <returns></returns>
    public int GetCostOfItemUpgrade(string itemName, int itemLevel)
    {
        if (itemLevel > 3)
            return 0;

        foreach (ItemList items in itemList)
        {
            if (itemName == items.item[0].GetComponent<Item>().ItemName)
            {
                return (int)items.cost[itemLevel - 1];
            }
        }

        return 0;
    }

    /// <summary>
    /// Returns prefab for the selcted item and its level
    /// </summary>
    /// <param name="itemName"></param>
    /// <param name="itemLevel"></param>
    /// <returns></returns>
    public GameObject GetItemPrefab(string itemName, int itemLevel)
    {
        foreach (ItemList items in itemList)
        {
            if (itemName == items.item[0].GetComponent<Item>().ItemName)
            {
                return items.item[itemLevel - 1];
            }
        }

        return null;
    }

    public string GetItemDescription(string itemName, int itemLevel)
    {
        foreach (ItemList items in itemList)
        {
            if (itemName == items.item[0].GetComponent<Item>().ItemName)
            {
                if (itemLevel == 3)
                    return items.item[2].GetComponent<Item>().Description;

                return items.item[itemLevel].GetComponent<Item>().Description;
            }
        }

        return "description";
    }

    #endregion
}
