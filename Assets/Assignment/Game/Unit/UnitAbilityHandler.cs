﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UnitAbilityHandler : MonoBehaviour
{
    public List<Ability> abilities;

    [SerializeField]
    UpgradeSystem upgradeSystem = null;

    [SerializeField]
    private GameObject abilityParent;

    public void ExecuteAbility(int number)
    {
        abilities[number].Execute();
    }

    // Use this for initialization
    void Awake ()
    {
        abilities = new List<Ability>();

        UpdateAbilityList();

        upgradeSystem = FindObjectOfType<UpgradeSystem>();

        //Debug.Log(abilities.Count);
	}

    private void OnLevelWasLoaded(int level)
    {
        if (upgradeSystem == null)
            upgradeSystem = FindObjectOfType<UpgradeSystem>();
    }

    public void ClearAbilityList()
    {
        abilities.Clear();

        //Find all the abilites on this unit
        Ability[] temp = abilityParent.GetComponentsInChildren<Ability>();

        foreach (Ability ability in temp)
        {
            Destroy(ability.gameObject);
        }
    }

    public void UpdateAbilityList()
    {
        abilities.Clear();

        //Find all the abilites on this unit
        Ability[] temp = abilityParent.GetComponentsInChildren<Ability>();

        foreach (Ability ability in temp)
        {
            abilities.Add(ability);
            ability.Owner = GetComponent<Unit>();
        }
    }

    public void UpgradeAbility(GameObject abilityPrefab, string abilityName)
    {
        //Find all the abilites on this unit
        Ability[] temp = GetComponentsInChildren<Ability>();

        foreach (Ability ability in temp)
        {
            if (ability.AbilityName == abilityName)
            {
                abilities.Remove(ability);
				Destroy(ability.gameObject);
                break;
            }
        }

		Instantiate(abilityPrefab, abilityParent.transform);

        // Update ability list
        Invoke("UpdateAbilityList", 0.1f);
    }

    [PunRPC]
    public void UpgradeAbilityNetwork(string abilityName, int upgradeLevel, int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GameObject abilityPrefab = upgradeSystem.GetAbilityPrefab(abilityName, upgradeLevel);

        //Find all the abilites on this unit
        Ability[] temp = GetComponentsInChildren<Ability>();

        foreach (Ability ability in temp)
        {
            if (ability.AbilityName == abilityName)
            {
                abilities.Remove(ability);
                Destroy(ability.gameObject);
                break;
            }
        }

        Instantiate(abilityPrefab, abilityParent.transform);

        // Update ability list
        Invoke("UpdateAbilityList", 0.1f);
    }

    public int GetAbilityLevelByName(string name)
    {
        foreach(Ability ability in abilities)
        {
            if (ability.AbilityName == name)
            {
                return ability.AbilityLevel;
            }
        }

        return 0;
    }

    /// <summary>
    /// returns the index position of given ability 
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public int GetAbilityNumberByName(string name)
    {
        for (int i = 0; i < abilities.Count; ++i)
        {
            if (abilities[i].AbilityName == name)
            {
                return i;
            }
        }

        return -1;
    }
}
