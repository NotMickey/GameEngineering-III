﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttributeType { Damage, Armor, Health, Speed, CastTime, AbilityRange, Cooldown}

public enum ModifierType { Additive, Percentage}

public class UnitAttributes : MonoBehaviour
{
    float[] ArmorModifier;
    float[] SpeedModifier;
    float[] CastTimeModifier;
    float[] AbilityRangeModifier;
    float[] CooldownModifier;

    #region Register/Remove modifiers

    public void RegisterModifier(AttributeType attribute, ModifierType modifier, float value)
    {
        switch (attribute)
        {
            case AttributeType.Armor:
                if (modifier == ModifierType.Additive)
                    ArmorModifier[0] += value;
                else
                    ArmorModifier[1] += value;
                break;
            case AttributeType.Speed:
                if (modifier == ModifierType.Additive)
                    SpeedModifier[0] += value;
                else
                    SpeedModifier[1] += value;
                break;
            case AttributeType.CastTime:
                if (modifier == ModifierType.Additive)
                    CastTimeModifier[0] += value;
                else
                    CastTimeModifier[1] += value;
                break;
            case AttributeType.AbilityRange:
                if (modifier == ModifierType.Additive)
                    AbilityRangeModifier[0] += value;
                else
                    AbilityRangeModifier[1] += value;
                break;
            case AttributeType.Cooldown:
                if (modifier == ModifierType.Additive)
                    CooldownModifier[0] += value;
                else
                    CooldownModifier[1] += value;
                break;

            default:
                break;
        }
    }

    public void RemoveModifier(AttributeType attribute, ModifierType modifier, float value)
    {
        switch (attribute)
        {
            case AttributeType.Armor:
                if (modifier == ModifierType.Additive)
                    ArmorModifier[0] -= value;
                else
                    ArmorModifier[1] -= value;
                break;
            case AttributeType.Speed:
                if (modifier == ModifierType.Additive)
                    SpeedModifier[0] -= value;
                else
                    SpeedModifier[1] -= value;
                break;
            case AttributeType.CastTime:
                if (modifier == ModifierType.Additive)
                    CastTimeModifier[0] -= value;
                else
                    CastTimeModifier[1] -= value;
                break;
            case AttributeType.AbilityRange:
                if (modifier == ModifierType.Additive)
                    AbilityRangeModifier[0] -= value;
                else
                    AbilityRangeModifier[1] -= value;
                break;
            case AttributeType.Cooldown:
                if (modifier == ModifierType.Additive)
                    CooldownModifier[0] -= value;
                else
                    CooldownModifier[1] -= value;
                break;

            default:
                break;
        }
    }

    #endregion

    #region Modify Values

    public float ModifyIncomingDamage(float amount)
    {
        if (ArmorModifier[0] == 0 && ArmorModifier[1] == 0)
            return amount;

        float modifiedAmount = amount - ArmorModifier[0];
        modifiedAmount -= (ArmorModifier[1] * modifiedAmount ) / 100;

        return modifiedAmount;
    }

    public float ModifySpeed(float amount)
    {
        if (SpeedModifier[0] == 0 && SpeedModifier[1] == 0)
            return amount;

        float modifiedAmount = amount + SpeedModifier[0];
        modifiedAmount += (SpeedModifier[1] * modifiedAmount) / 100;

        return modifiedAmount;
    }

    public float ModifyAbilityRange(float amount)
    {
        if (AbilityRangeModifier[0] == 0 && AbilityRangeModifier[1] == 0)
            return amount;

        float modifiedAmount = amount + AbilityRangeModifier[0];
        modifiedAmount += (AbilityRangeModifier[1] * modifiedAmount) / 100;

        return modifiedAmount;
    }

    public float ModifyCooldown(float amount)
    {
        if (CooldownModifier[0] == 0 && CooldownModifier[1] == 0)
            return amount;

        float modifiedAmount = amount - CooldownModifier[0];
        modifiedAmount -= (AbilityRangeModifier[1] * modifiedAmount) / 100;

        return modifiedAmount;
    }

    #endregion

    // Use this for initialization
    void Start ()
    {
        ArmorModifier = new float[2];
        SpeedModifier = new float[2];
        CastTimeModifier = new float[2];
        AbilityRangeModifier = new float[2];
        CooldownModifier = new float[2];
    }
}
