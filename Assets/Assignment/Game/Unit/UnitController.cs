﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class UnitController : MonoBehaviour, IUnitController, IPunObservable {

    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    public Rigidbody rb = null;

    [SerializeField]
    private SkillCastHandler handler = null;

    [SerializeField]
    private Vector3? moveTarget;
    public bool HasReachedDestination { get { return !moveTarget.HasValue; } }

    [NonSerialized]
    public bool isIncpacitated;

    [SerializeField]
    private float moveSpeed = 10f;

    [SerializeField]
    private float moveTargetTolerance = 0.1f;

    [SerializeField]
    private float turnSpeed = 360f;

    [SerializeField]
    private float maxMoveAngle = 45f;

    [SerializeField]
    private Map map = null;

    [SerializeField]
    private string forwardAnimation = "Forward";

    private GameObject ragDoll;
    public GameObject RagDoll { get { return ragDoll; } set { ragDoll = value; } }

    private void Start()
    {
        map = FindObjectOfType<Map>();
    }

    protected virtual void Update()
    {
        UpdateRotation();
        UpdateTranslation();
        UpdateMapPosition();
    }

    // this method makes sure the unit is on the ground, not hovering in the air or below the map
    private void UpdateMapPosition()
    {
        Vector3 mapPos, mapNormal;

        if (map == null)
            map = FindObjectOfType<Map>();
        map.GetMapPointFromWorldPoint(transform.position, out mapPos, out mapNormal);
        transform.position = mapPos;
        Vector3 right = transform.right;
        Vector3 up = mapNormal;
        Vector3 forward = Vector3.Cross(right, up);
        transform.rotation = Quaternion.LookRotation(forward, up);
    }

    private void UpdateRotation()
    {
        if (moveTarget.HasValue && !isIncpacitated)
        {
            Vector3 delta = (moveTarget.Value - transform.position);
            Vector3 curDir = transform.forward;
            Vector3 targetDir = delta.normalized;
            
            transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);
        }
    }

    private void UpdateTranslation()
    {
        if (moveTarget.HasValue && !isIncpacitated)
        {
            rb.isKinematic = false;
            Vector3 curDir = transform.forward;
            Vector3 curPos = transform.position;
            Vector3 delta = (moveTarget.Value - curPos);
            float dist = delta.magnitude;
            if (dist > moveTargetTolerance)
            {
                float modSpeed = GetComponent<Unit>().attributes.ModifySpeed(moveSpeed);

                Vector3 move = curDir * modSpeed * Time.deltaTime;
                Vector3 newPos = curPos + move;
                Vector3 mapPos, mapNormal;
                map.GetMapPointFromWorldPoint(newPos, out mapPos, out mapNormal);
                float terrainAngle = Vector3.Angle(Vector3.up, mapNormal);
                if (terrainAngle <= maxMoveAngle)
                {
                    transform.position = mapPos;
                    animator.SetFloat(forwardAnimation, 1f);
                }
            }
            else
            {
                animator.SetFloat(forwardAnimation, 0);
                moveTarget = null;
                OnDestinationReached();
            }
        }
    }

    protected virtual void OnDestinationReached()
    {
        rb.isKinematic = true;
        //Debug.Log("reached");
    }

    public void MoveTo(Vector3 targetPosition)
    {
        moveTarget = targetPosition;
    }

    public void StopMoving()
    {
        moveTarget = null;
        rb.isKinematic = true;
        animator.SetFloat(forwardAnimation, 0);
    }

    public void StopAll()
    {
        StopMoving();
        Interrupt(true);
    }

    public void ApplyForce(Vector3 i_forceOrigin, float i_magnitude)
    {
        Vector3 direction = (rb.position - i_forceOrigin).normalized;
        Vector3 endPosition = rb.position + direction * i_magnitude;

        StartCoroutine(PushUnit(rb.position, endPosition));
    }

    public void Interrupt(bool wasCancelled = false)
    {
        if (wasCancelled)
            animator.SetTrigger("IsReset");
        else
            animator.SetTrigger("IsHit");

        // This will probably change
        handler.SkillInterruptedEvent();
    }

    IEnumerator StunUnit(float i_stunDuration)
    {
        yield return new WaitForSeconds(i_stunDuration);

        isIncpacitated = false;
    }

    IEnumerator PushUnit(Vector3 i_startPosition, Vector3 i_endPosition)
    {
        float endTime = 0.3f;
        float startTime = 0.0f;

        while (startTime < endTime)
        {
            rb.position = Vector3.Lerp(i_startPosition, i_endPosition, (startTime / endTime));
            startTime += Time.deltaTime;

            yield return null;
        }

        StopAll();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(rb.position);
            stream.SendNext(rb.rotation);
        }
        else
        {
            rb.position = (Vector3)stream.ReceiveNext();
            rb.rotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
