﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItem
{
    string ItemName { get; }
    int ItemLevel { get; }
    string Description { get; }
    Unit Owner { get; set; }

    void SetModifiers();
    void RemoveModifiers();
}
