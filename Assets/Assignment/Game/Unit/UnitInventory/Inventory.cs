﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    int maxItems;

    [SerializeField]
    private GameObject itemParent;

    [SerializeField]
    UpgradeSystem upgradeSystem = null;

    public List<Item> items;

    public bool IsSpaceAvailable()
    {
        if (items.Count < maxItems)
            return true;

        return false;
    }

    public void ClearInventory()
    {
        if (items.Count > 0)
        {
            foreach (Item item in items)
            {
                item.RemoveModifiers();
            }
        }

        items.Clear();

        //Find all the items on this unit
        Item[] temp = itemParent.GetComponentsInChildren<Item>();

        foreach (Item item in temp)
        {
            Destroy(item.gameObject);
        }
    }

    public void UpdateInventory()
    {
        if (items.Count > 0)
        {
            foreach (Item item in items)
            {
                item.RemoveModifiers();
            }
        }

        items.Clear();

        //Find all the items on this unit
        Item[] temp = itemParent.GetComponentsInChildren<Item>();

        foreach (Item item in temp)
        {
            if (!IsSpaceAvailable())
            {
                Debug.Log("Warning not enough space! Item will not be registered");
                return;
            }
                
            items.Add(item);
            item.Owner = GetComponent<Unit>();
            item.SetModifiers();
        }
    }

    public void UpgradeItem(GameObject itemPrefab, string itemName)
    {
        //Find all the abilites on this unit
        Item[] temp = itemParent.GetComponentsInChildren<Item>();

        foreach (Item item in temp)
        {
            if (item.ItemName == itemName)
            {
                items.Remove(item);
                item.RemoveModifiers();
                Destroy(item.gameObject);
                break;
            }
        }

        Instantiate(itemPrefab, itemParent.transform);

        // Update ability list
        Invoke("UpdateInventory", 0.1f);
    }

    [PunRPC]
    public void UpgradeItemNetwork(string itemName, int upgradeLevel, int viewID)
    {
        if (viewID != GetComponent<PhotonView>().ViewID)
            return;

        GameObject itemPrefab = upgradeSystem.GetItemPrefab(name, upgradeLevel);

        //Debug.Log(itemPrefab);

        //Find all the abilites on this unit
        Item[] temp = itemParent.GetComponentsInChildren<Item>();

        foreach (Item item in temp)
        {
            if (item.ItemName == itemName)
            {
                items.Remove(item);
                item.RemoveModifiers();
                Destroy(item.gameObject);
                break;
            }
        }

        Instantiate(itemPrefab, itemParent.transform);

        // Update ability list
        Invoke("UpdateInventory", 0.1f);
    }

    public int GetItemLevelByName(string name)
    {
        foreach (Item item in items)
        {
            if (item.ItemName == name)
            {
                //Debug.Log("Item found lv = " + item.ItemLevel);

                return item.ItemLevel;
            }
        }

        //Debug.Log("No item found return 0");

        return 0;
    }

    private void Awake()
    {
        items = new List<Item>();

        upgradeSystem = FindObjectOfType<UpgradeSystem>();
    }

    private void OnLevelWasLoaded(int level)
    {
        if (upgradeSystem == null)
            upgradeSystem = FindObjectOfType<UpgradeSystem>();
    }
}
