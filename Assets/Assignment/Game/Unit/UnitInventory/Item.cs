﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour, IItem
{
    public string ItemName { get { return name; } protected set { name = value; } }

    public int ItemLevel { get { return itemLevel; } }

    public Unit Owner { get { return owner; } set { owner = value; } }

    public string Description { get { return itemDescription; } }

    [SerializeField]
    private string itemName = "default";

    [SerializeField]
    private int itemLevel = 0;

    [SerializeField]
    private string itemDescription = "Description goes here";

    private Unit owner = null;

    public virtual void SetModifiers() { }
    public virtual void RemoveModifiers() { }
}
