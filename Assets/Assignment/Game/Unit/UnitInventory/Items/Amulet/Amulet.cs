﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amulet : Item
{
    [SerializeField]
    float cooldownDecrease;

    Amulet (Unit owner)
    {
        Owner = owner;
    }

    public override void SetModifiers()
    {
        Owner.attributes.RegisterModifier(AttributeType.Cooldown, ModifierType.Percentage, cooldownDecrease);
    }

    public override void RemoveModifiers()
    {
        Owner.attributes.RemoveModifier(AttributeType.Cooldown, ModifierType.Percentage, cooldownDecrease);
    }

    private void Start()
    {
        ItemName = "Amulet";
    }

}
