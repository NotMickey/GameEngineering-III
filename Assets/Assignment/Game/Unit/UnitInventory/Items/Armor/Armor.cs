﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item
{
    [SerializeField]
    float normalDamageReduction;

    [SerializeField]
    float lavaDamageReduction;

    Armor(Unit owner)
    {
        Owner = owner;
    }

    public override void SetModifiers()
    {
        Owner.attributes.RegisterModifier(AttributeType.Armor, ModifierType.Percentage, normalDamageReduction);
    }

    public override void RemoveModifiers()
    {
        Owner.attributes.RemoveModifier(AttributeType.Armor, ModifierType.Percentage, normalDamageReduction);
    }

    private void Start()
    {
        ItemName = "Armor";
    }
}
