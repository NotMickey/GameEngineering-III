﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boots : Item
{

    [SerializeField]
    float speedIncrease;

    Boots(Unit owner)
    {
        Owner = owner;
    }

    public override void SetModifiers()
    {
        Owner.attributes.RegisterModifier(AttributeType.Speed, ModifierType.Additive, speedIncrease);
    }

    public override void RemoveModifiers()
    {
        Owner.attributes.RemoveModifier(AttributeType.Speed, ModifierType.Additive, speedIncrease);
    }

    private void Start()
    {
        ItemName = "Boots";
    }

}
