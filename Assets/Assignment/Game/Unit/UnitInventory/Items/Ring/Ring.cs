﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : Item
{

    [SerializeField]
    float hpRegen;

    Ring(Unit owner)
    {
        Owner = owner;
    }

    private void Update()
    {
        if (time < 1.0f)
        {
            time += Time.deltaTime;
        }
        else
        {
            time = 0.0f;
            damage.ApplyDamage(healthComponent, hpRegen);
        }
    }

    private void Start()
    {
        ItemName = "Ring";
        healthComponent = GetComponentInParent<Health>();
    }

    float time = 0.0f;
    Health healthComponent;

    [SerializeField]
    private Damage damage;
}
