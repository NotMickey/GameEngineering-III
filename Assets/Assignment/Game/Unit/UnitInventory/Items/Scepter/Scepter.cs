﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scepter : Item
{
    [SerializeField]
    float rangeIncrease;

    Scepter(Unit owner)
    {
        Owner = owner;
    }

    public override void SetModifiers()
    {
        Owner.attributes.RegisterModifier(AttributeType.AbilityRange, ModifierType.Percentage, rangeIncrease);
    }

    public override void RemoveModifiers()
    {
        Owner.attributes.RemoveModifier(AttributeType.AbilityRange, ModifierType.Percentage, rangeIncrease);
    }

    private void Start()
    {
        ItemName = "Scepter";
    }
}
