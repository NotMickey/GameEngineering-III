﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class App : MonoBehaviour {

    private static App _instance = null;

    [SerializeField]
    private Scenes scenes = null;
    public Scenes Scenes { get { return scenes; } }

    public static App Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start() {
        //DontDestroyOnLoad(gameObject);
    }

    public void Exit() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }


	
}
