﻿using UnityEngine;

public interface IPlayer {

	Color Color { get; }
	string Name { get; set; }

    bool IsLocal { get; }

}

