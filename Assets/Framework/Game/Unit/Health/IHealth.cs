﻿public interface IHealth {

    float Current { get; set; }
    float Max { get; }
    float Percent { get; }  /* 0 to 1 */

    bool IsAlive { get; set; }
    bool IsDead { get; set; }

    void SetStats(IHealthStatsInfo stats);

    void Damage(IDamageInfo damageInfo);

}
