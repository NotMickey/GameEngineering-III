﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour, IAppAware {

    [SerializeField]
    private App app = null;
    public App App { get { return app; } set { app = value; } }

    public void StartGame() {
        SceneManager.LoadScene("SelectionMenu");
    }

    public void ExitGame() {
        app.Exit();
    }
	
}
