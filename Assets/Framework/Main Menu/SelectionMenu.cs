﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionMenu : MonoBehaviour {

    public void StartAIGame()
    {
        App.Instance.Scenes.LoadGame();
    }

    public void StartPlayerGame()
    {
        SceneManager.LoadScene("Lobby");
    }
}
